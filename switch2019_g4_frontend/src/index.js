import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import AppProvider from './context/AppProvider';
import * as serviceWorker from './serviceWorker';
import PersonProvider from "./context/PersonProvider";
import GroupProvider from "./context/GroupProvider";
import SnackbarProvider from "./context/SnackbarProvider";
import AppSnackbar from "./components_experiments/Snackbar";

ReactDOM.render(
    <AppProvider>
        <SnackbarProvider>
            <PersonProvider>
                <GroupProvider>
                    <React.StrictMode>
                        <AppSnackbar/>
                        <App/>
                    </React.StrictMode>
                </GroupProvider>
            </PersonProvider>
        </SnackbarProvider>
    </AppProvider>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
