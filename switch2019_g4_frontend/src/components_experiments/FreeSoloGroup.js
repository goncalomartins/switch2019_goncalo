import React, {useContext} from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import {changeTransactionsFiltersAccountID} from "../context/GroupActions";
import GroupContext from "../context/GroupContext";

export default function FreeSolo() {
    const accounts = useContext(GroupContext).state.accounts.data;
    const {dispatch} = useContext(GroupContext);

    function handleChange(event, value) {
        dispatch(changeTransactionsFiltersAccountID(value))
    }

    return (
        <div style={{width: 300}}>
            <Autocomplete
                freeSolo
                id="free-solo-2-demo"
                disableClearable
                options={accounts.map((option) => option.accountID)}
                onChange={handleChange}
                renderInput={(params) => (
                    <TextField
                        {...params}
                        label="Account"
                        margin="normal"
                        variant="outlined"
                        InputProps={{...params.InputProps, type: 'search'}}
                    />
                )}
            />
        </div>
    );
}