export const URL_API = 'http://localhost:8080/';

export const FETCH_GROUP_TRANSACTIONS_STARTED = 'FETCH_GROUP_TRANSACTIONS_STARTED';
export const FETCH_GROUP_TRANSACTIONS_SUCCESS = 'FETCH_GROUP_TRANSACTIONS_SUCCESS';
export const FETCH_GROUP_TRANSACTIONS_FAILURE = 'FETCH_GROUP_TRANSACTIONS_FAILURE';
export const FETCH_GROUP_TRANSACTIONS_AFTER_POST = 'FETCH_GROUP_TRANSACTIONS_AFTER_POST';

export function fetchGroupTransactionsStarted() {
    return {
        type: FETCH_GROUP_TRANSACTIONS_STARTED,
    }
}

export function fetchGroupTransactionsSuccess(transactions) {
    return {
        type: FETCH_GROUP_TRANSACTIONS_SUCCESS,
        payload: {
            data: [...transactions]
        }
    }
}

export function fetchGroupTransactionsFailure(message) {
    return {
        type: FETCH_GROUP_TRANSACTIONS_FAILURE,
        payload: {
            error: message
        }
    }
}

export function fetchGroupTransactionsAfterPost() {
    return {
        type: FETCH_GROUP_TRANSACTIONS_AFTER_POST,
    }
}

//Actions for Fetch Group Categories

export const FETCH_GROUP_CATEGORIES_STARTED = 'FETCH_GROUP_CATEGORIES_STARTED';
export const FETCH_GROUP_CATEGORIES_SUCCESS = 'FETCH_GROUP_CATEGORIES_SUCCESS';
export const FETCH_GROUP_CATEGORIES_FAILURE = 'FETCH_GROUP_CATEGORIES_FAILURE';
export const FETCH_GROUP_CATEGORIES_AFTER_POST = 'FETCH_GROUP_CATEGORIES_AFTER_POST';

export function fetchGroupCategoriesStarted() {
    return {
        type: FETCH_GROUP_CATEGORIES_STARTED,

    }
}

export function fetchGroupCategoriesSuccess(categories) {
    return {
        type: FETCH_GROUP_CATEGORIES_SUCCESS,
        payload: {
            data:
                [...categories]
        }

    }
}

export function fetchGroupCategoriesFailure(message) {
    return {
        type: FETCH_GROUP_CATEGORIES_FAILURE,
        payload: {
            error: message
        }
    }
}

export function fetchGroupCategoriesAfterPost() {
    return {
        type: FETCH_GROUP_CATEGORIES_AFTER_POST,
    }
}

//-------------------------- FETCH_GROUP_ACCOUNTS -------------------------
export const FETCH_GROUP_ACCOUNTS_STARTED = 'FETCH_GROUP_ACCOUNTS_STARTED';
export const FETCH_GROUP_ACCOUNTS_SUCCESS = 'FETCH_GROUP_ACCOUNTS_SUCCESS';
export const FETCH_GROUP_ACCOUNTS_FAILURE = 'FETCH_GROUP_ACCOUNTS_FAILURE';
export const FETCH_GROUP_ACCOUNTS_AFTER_POST = 'FETCH_GROUP_ACCOUNTS_AFTER_POST';

export function fetchGroupAccountsStarted() {
    return {
        type: FETCH_GROUP_ACCOUNTS_STARTED,

    }
}

export function fetchGroupAccountsSuccess(accounts) {
    console.log("here groupAccounts");
    console.log(accounts);
    return {
        type: FETCH_GROUP_ACCOUNTS_SUCCESS,
        payload: {
            data:
                [...accounts]
        }

    }
}

export function fetchGroupAccountsFailure(message) {
    return {
        type: FETCH_GROUP_ACCOUNTS_FAILURE,
        payload: {
            error: message
        }
    }
}

export function fetchGroupAccountsAfterPost() {
    return {
        type: FETCH_GROUP_ACCOUNTS_AFTER_POST,
    }
}

//Actions for Fetch Group's Members

export const FETCH_GROUP_MEMBERS_STARTED = 'FETCH_GROUP_MEMBERS_STARTED';
export const FETCH_GROUP_MEMBERS_SUCCESS = 'FETCH_GROUP_MEMBERS_SUCCESS';
export const FETCH_GROUP_MEMBERS_FAILURE = 'FETCH_GROUP_MEMBERS_FAILURE';
export const FETCH_GROUP_MEMBERS_AFTER_POST = 'FETCH_GROUP_MEMBERS_AFTER_POST';

export function fetchGroupMembersStarted() {
    return {
        type: FETCH_GROUP_MEMBERS_STARTED,

    }
}

export function fetchGroupMembersSuccess(members) {
    return {
        type: FETCH_GROUP_MEMBERS_SUCCESS,
        payload: {
            data:
                [...members]
        }

    }
}

export function fetchGroupMembersFailure(message) {
    return {
        type: FETCH_GROUP_MEMBERS_FAILURE,
        payload: {
            error: message
        }
    }
}

export function fetchGroupMembersAfterPost() {
    return {
        type: FETCH_GROUP_MEMBERS_AFTER_POST,
    }

}

//-------------------------- FILTER_TRANSACTIONS -------------------------

export const FILTER_TRANSACTIONS = 'FILTER_TRANSACTIONS';
export const TRANSACTIONS_ACCOUNTID_FILTER = 'TRANSACTIONS_ACCOUNTID_FILTER';
export const TRANSACTIONS_INITIALDATE_FILTER = 'TRANSACTIONS_INITIALDATE_FILTER';
export const TRANSACTIONS_FINALDATE_FILTER = 'TRANSACTIONS_FINALDATE_FILTER';
export const CLEAR_TRANSACTIONS_FILTERS = 'CLEAR_TRANSACTIONS_FILTERS';

export function changeTransactionsFiltersAccountID(accountID) {
    return {
        type: TRANSACTIONS_ACCOUNTID_FILTER,
        accountID: accountID
    }
}

export function changeTransactionsFiltersInitialDate(initialDate) {
    return {
        type: TRANSACTIONS_INITIALDATE_FILTER,
        initialDate: initialDate
    }
}

export function changeTransactionsFiltersFinalDate(finalDate) {
    return {
        type: TRANSACTIONS_FINALDATE_FILTER,
        finalDate: finalDate
    }
}

export function filterTransactions() {
    return {
        type: FILTER_TRANSACTIONS,
    }
}

export function clearTransactionsFilters() {
    return {
        type: CLEAR_TRANSACTIONS_FILTERS,
    }
}

//-------------------------- FETCH_GROUP_INFO -------------------------

export const FETCH_GROUP_INFO_STARTED = 'FETCH_GROUP_INFO_STARTED';
export const FETCH_GROUP_INFO_SUCCESS = 'FETCH_GROUP_INFO_SUCCESS';
export const FETCH_GROUP_INFO_FAILURE = 'FETCH_GROUP_INFO_FAILURE';

export function fetchGroupInfoStarted() {
    return {
        type: FETCH_GROUP_INFO_STARTED,
    }
}

export function fetchGroupInfoSuccess(info) {
    return {
        type: FETCH_GROUP_INFO_SUCCESS,
        payload: {
            data: {...info}
        }
    }
}

export function fetchGroupInfoFailure(message) {
    return {
        type: FETCH_GROUP_INFO_FAILURE,
        payload: {
            error: message
        }
    }
}


