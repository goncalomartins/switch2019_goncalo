import React, {useReducer} from 'react';
import PropTypes from "prop-types";
import {Provider} from './SnackbarContext';
import snackbarReducer from './SnackbarReducer';

const initialState = {
    snackbarOpened: false,
    message: "",
};

const SnackbarProvider = (props) => {
    const [state, dispatch] = useReducer(snackbarReducer, initialState);
    return (
        <Provider value={{
            state,
            dispatch
        }}>
            {props.children}
        </Provider>
    );
};
SnackbarProvider.propTypes = {
    children: PropTypes.node,
};


export default SnackbarProvider;
