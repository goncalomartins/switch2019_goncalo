import React, {useReducer} from "react";
import {Provider} from './PersonContext'
import personReducer from "./PersonReducer";
import PropTypes from "prop-types";

const initialState = {
    info: {
        loading: true,
        error: null,
        data: {},
    },
    groups: {
        loading: true,
        error: null,
        data: [],
        isSubmitted: 'no'
    },
    transactions: {
        loading: false,
        error: null,
        data: []
    },
    categories: {
        loading: true,
        error: null,
        data: [],
        isSubmitted: 'no'
    },
    accounts: {
        loading: true,
        error: null,
        data: [],
        isSubmitted: 'no'
    },
    transactionsFilters: {
        hasFilters: 'no',
        accountID: null,
        initialDate: '',
        finalDate: ''
    },
    groupId: null,
    description: ''
};

const headCellsGroups = [
    {id: 'groupID', numeric: false, disablePadding: true, label: 'Group Id'},
    {id: 'description', numeric: false, disablePadding: true, label: 'Description'},
    {id: 'creationDate', numeric: false, disablePadding: true, label: 'Creation Date'},
];

const headCellsCategories = [
    {id: 'designation', numeric: false, disablePadding: true, label: 'Designation'}
];

const headCellsTransactions = [
    {id: 'dateTime', numeric: true, disablePadding: true, label: 'Date'},
    {id: 'description', numeric: false, disablePadding: true, label: 'Description'},
    {id: 'category', numeric: true, disablePadding: true, label: 'Category'},
    {id: 'creditAccountID', numeric: true, disablePadding: true, label: 'Credit Account'},
    {id: 'debitAccountID', numeric: true, disablePadding: true, label: 'Debit Account'},
    {id: 'type', numeric: true, disablePadding: true, label: 'Type'},
    {id: 'amount', numeric: true, disablePadding: true, label: 'Amount'},
];

const headCellsAccounts = [
    {id: 'accountID', numeric: false, disablePadding: true, label: 'Account Id'},
    {id: 'denomination', numeric: false, disablePadding: true, label: 'Denomination'},
    {id: 'description', numeric: false, disablePadding: true, label: 'Description'},
];

const categoriesHeaders = {
    designation: "Designation"
};

const groupsHeaders = {
    id: "ID",
    description: "Description",
    creationDate: "Creation Date"
};


const transactionsHeaders = {
    amount: "Amount",
    dateTime: "Date",
    type: "Type",
    description: "Description",
    category: "Category",
    debitAccount: "Debit Account ID",
    creditAccount: "Credit Account ID",
};

const accountsHeaders = {
    id: "ID",
    denomination: "Denomination",
    description: "Description"

};

const PersonProvider = (props) => {
    const [state, dispatch] = useReducer(personReducer, initialState);
    return (
        <Provider value={{
            state,
            headCellsGroups,
            headCellsTransactions,
            headCellsCategories,
            headCellsAccounts,
            groupsHeaders,
            transactionsHeaders,
            categoriesHeaders,
            accountsHeaders,
            dispatch
        }}>
            {props.children}
        </Provider>
    )
};

PersonProvider.propTypes = {
    children: PropTypes.node,
};

export default PersonProvider