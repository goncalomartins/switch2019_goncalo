import React from 'react';
import {useHistory} from 'react-router-dom';
import AppContext from './context/AppContext';
import {logout} from './context/AppActions';
import Button from "@material-ui/core/Button";

function Logout() {
    const {dispatch} = React.useContext(AppContext);
    let history = useHistory();
    const handleClick = () => {
        history.push("/login");
        dispatch(logout());
//       
    };

    return (<Button color="inherit" type="submit" onClick={handleClick}>Logout</Button>);

}

export default Logout;