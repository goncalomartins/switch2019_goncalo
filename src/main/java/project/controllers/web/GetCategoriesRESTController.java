package project.controllers.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import project.dto.CategoriesDTO;
import project.frameworkddd.IUSGetCategoriesService;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class GetCategoriesRESTController {

    @Autowired
    private IUSGetCategoriesService service;

    /**
     * Method getCategoriesByGroupID
     *
     * @param groupID groupID in the Path
     * @return ResponseEntity and HttpStatus
     */
    @GetMapping("/groups/{groupID}/categories")
    public ResponseEntity<Object> getCategoriesByGroupID(@PathVariable String groupID) {
        CategoriesDTO result = service.getCategoriesByGroupID(groupID);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    /**
     * Method getCategoriesByPersonID
     *
     * @param personID groupID in the Path
     * @return ResponseEntity and HttpStatus
     */
    @GetMapping("/people/{personID}/categories")
    public ResponseEntity<Object> getCategoriesByPersonID(@PathVariable String personID) {
        CategoriesDTO result = service.getCategoriesByPersonID(personID);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
