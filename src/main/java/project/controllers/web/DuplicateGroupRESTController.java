package project.controllers.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import project.dto.DuplicateGroupAssembler;
import project.dto.DuplicateGroupInfoDTO;
import project.dto.DuplicateGroupRequestDTO;
import project.dto.DuplicateGroupResponseDTO;
import project.frameworkddd.IUSDuplicateGroupService;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class DuplicateGroupRESTController {

    @Autowired
    private IUSDuplicateGroupService service;

    @PostMapping("people/{personEmail}/groups/duplicate")
    public ResponseEntity<Object> duplicateGroup(@RequestBody DuplicateGroupInfoDTO info, @PathVariable String personEmail) {

        DuplicateGroupRequestDTO requestDTO = DuplicateGroupAssembler.mapToRequestDTO(info.getGroupID1(), info.getGroupID2(), info.getLedgerID(), personEmail);
        DuplicateGroupResponseDTO result = service.duplicateGroup(requestDTO);

        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }
}
