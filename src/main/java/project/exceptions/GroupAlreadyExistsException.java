package project.exceptions;

import static project.exceptions.Messages.GROUPALREADYEXISTS;

public class GroupAlreadyExistsException extends RuntimeException {
    static final long serialVersionUID = -6434624893433243517L;

    /**
     * Exception for when group already exists
     * Uses exceptions.Messages for exception message
     */
    public GroupAlreadyExistsException() {
        super(GROUPALREADYEXISTS);
    }

    /**
     * Exception for when group already exists
     * Uses provided message for exception message
     */
    public GroupAlreadyExistsException(String message) {
        super(message);
    }
}
