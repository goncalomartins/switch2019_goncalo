package project.exceptions;

import static project.exceptions.Messages.GROUPCONFLICT;

public class GroupConflictException extends RuntimeException {
    static final long serialVersionUID = -7455405598917654301L;

    /**
     * Exception for when group conflict occurs
     * Uses provided message for exception message
     */
    public GroupConflictException() {
        super(GROUPCONFLICT);
    }

    /**
     * Exception for when group conflict occurs
     * Uses provided message for exception message
     */
    public GroupConflictException(String errorMessage) {
        super(errorMessage);
    }


}
