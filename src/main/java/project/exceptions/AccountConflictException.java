package project.exceptions;

import static project.exceptions.Messages.ACCOUNTCONFLICT;

public class AccountConflictException extends RuntimeException {
    static final long serialVersionUID = -5212192373090025465L;

    /**
     * Exception for when account already exists
     * Uses exceptions.Messages for exception message
     */
    public AccountConflictException() {
        super(ACCOUNTCONFLICT);
    }

    /**
     * Exception for when account already exists
     * Uses provided message exception message
     */
    public AccountConflictException(String message) {
        super(message);
    }
}
