package project.frameworkddd;

import project.dto.DuplicateGroupRequestDTO;
import project.dto.DuplicateGroupResponseDTO;

public interface IUSDuplicateGroupService {

    DuplicateGroupResponseDTO duplicateGroup(DuplicateGroupRequestDTO requestDTO);
}
