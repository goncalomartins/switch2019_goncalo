package project.frameworkddd;

import project.dto.CreateCategoryForGroupRequestDTO;
import project.dto.CreateCategoryForGroupResponseDTO;

public interface IUSCreateCategoryForGroupService {

    CreateCategoryForGroupResponseDTO createCategoryForGroup(CreateCategoryForGroupRequestDTO requestDTO);

}
