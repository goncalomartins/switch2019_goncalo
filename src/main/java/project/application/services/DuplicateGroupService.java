package project.application.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.dto.DuplicateGroupAssembler;
import project.dto.DuplicateGroupRequestDTO;
import project.dto.DuplicateGroupResponseDTO;
import project.exceptions.GroupAlreadyExistsException;
import project.exceptions.GroupConflictException;
import project.exceptions.GroupLedgerAlreadyExistsException;
import project.exceptions.GroupNotFoundException;
import project.frameworkddd.IUSDuplicateGroupService;
import project.model.entities.group.Group;
import project.model.entities.group.GroupID;
import project.model.entities.ledger.Ledger;
import project.model.entities.shared.Category;
import project.model.entities.shared.LedgerID;
import project.model.entities.shared.PersonID;
import project.model.specifications.repositories.GroupRepository;
import project.model.specifications.repositories.LedgerRepository;

import java.util.Optional;

@Service
public class DuplicateGroupService implements IUSDuplicateGroupService {

    @Autowired
    GroupRepository groupRepository;

    @Autowired
    LedgerRepository ledgerRepository;

    @Override
    public DuplicateGroupResponseDTO duplicateGroup(DuplicateGroupRequestDTO requestDTO) {

        DuplicateGroupResponseDTO result;
        PersonID personID = new PersonID(requestDTO.getPersonEmail());
        GroupID groupID1 = new GroupID(requestDTO.getGroupID1());
        GroupID groupID2 = new GroupID(requestDTO.getGroupID2());
        LedgerID ledgerID = new LedgerID(requestDTO.getLedgerID());

        Optional<Group> opGroup1 = groupRepository.findById(groupID1);
        if (!opGroup1.isPresent()) {
            throw new GroupNotFoundException();
        }
        Group group1 = opGroup1.get();

        if (!group1.hasMemberID(personID)) {
            throw new GroupConflictException("The person with " + personID + " is not member of the Group");
        }

        Optional<Group> opGroup2 = groupRepository.findById(groupID2);
        if (opGroup2.isPresent()) {
            throw new GroupAlreadyExistsException();
        }
        Optional<Ledger> opLedger = ledgerRepository.findById(ledgerID);
        if (opLedger.isPresent()) {
            throw new GroupLedgerAlreadyExistsException();
        }

        Ledger newLedger = new Ledger(ledgerID);
        Group duplicatedGroup = new Group(groupID2, group1.getDescription().getDescriptionValue(), group1.getCreationDate().toStringDTO(), group1.getCreatorID(), ledgerID);

        for (PersonID memberID : group1.getMembersIDs().getPersonIDs()) {
            duplicatedGroup.addMemberID(memberID);
        }
        for (PersonID adminID : group1.getManagersIDs().getPersonIDs()) {
            duplicatedGroup.addManagerID(adminID);
        }
        for (Category category : group1.getCategories().getCategoriesValue()) {
            duplicatedGroup.addCategory(category.toStringDTO());
        }

        Group savedDuplicatedGroup = groupRepository.save(duplicatedGroup);
        ledgerRepository.save(newLedger);
        result = DuplicateGroupAssembler.mapToResponseDTO(savedDuplicatedGroup);

        return result;
    }
}
