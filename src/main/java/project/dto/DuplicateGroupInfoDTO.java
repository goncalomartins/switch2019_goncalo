package project.dto;

public class DuplicateGroupInfoDTO {

    private String groupID1;
    private String groupID2;
    private String ledgerID;

    public DuplicateGroupInfoDTO(String groupID1, String groupID2, String ledgerID) {
        setGroupID1(groupID1);
        setGroupID2(groupID2);
        setLedgerID(ledgerID);
    }

    public String getGroupID1() {
        return groupID1;
    }

    private void setGroupID1(String groupID1) {
        this.groupID1 = groupID1;
    }

    public String getGroupID2() {
        return groupID2;
    }

    private void setGroupID2(String groupID2) {
        this.groupID2 = groupID2;
    }

    public String getLedgerID() {
        return ledgerID;
    }

    private void setLedgerID(String ledgerID) {
        this.ledgerID = ledgerID;
    }
}
