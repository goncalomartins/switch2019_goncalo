package project.dto;

import java.util.Objects;

public class AddMemberRequestDTO {

    private String newMemberEmail;
    private String groupID;

    /**
     * Constructor method
     *
     * @param newMemberEmail ID of the new member to be added to group
     * @param groupID        ID of the group to which the new member is to be added
     */
    public AddMemberRequestDTO(String newMemberEmail, String groupID) {
        setNewMemberEmail(newMemberEmail);
        setGroupID(groupID);
    }

    /**
     * Get method
     *
     * @return ID of the new member to be added to group
     */
    public String getNewMemberEmail() {
        return newMemberEmail;
    }

    /**
     * Set method
     *
     * @param newMemberEmail ID of the new member to be added to group
     */
    private void setNewMemberEmail(String newMemberEmail) {
        this.newMemberEmail = newMemberEmail;
    }

    /**
     * Get method
     *
     * @return ID of the group to which the new member is to be added
     */
    public String getGroupID() {
        return groupID;
    }

    /**
     * Set method
     *
     * @param groupID ID of the group to which the new member is to be added
     */
    private void setGroupID(String groupID) {
        this.groupID = groupID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AddMemberRequestDTO that = (AddMemberRequestDTO) o;
        return Objects.equals(newMemberEmail, that.newMemberEmail) &&
                Objects.equals(groupID, that.groupID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(newMemberEmail, groupID);
    }
}
