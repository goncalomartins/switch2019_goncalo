package project.dto;

import java.util.Objects;

public class CreateAccountForGroupInfoDTO {
    final String accountID;
    final String denomination;
    final String description;

    /**
     * Constructor of CreateAccountForGroupInfoDTO class
     *
     * @param accountID The identity of the new account to be created as a String
     * @param denomination The denomination of the new account to be created as a String
     * @param description The description of the new account to be created as a String
     */
    public CreateAccountForGroupInfoDTO(String accountID, String denomination, String description){
        this.accountID=accountID;
        this.denomination=denomination;
        this.description= description;
    }

    /**
     * getAccountID method
     * gets the accountID attribute from infoDTO
     *
     * @return accountID as a String
     */
    public String getAccountID() {
        return accountID;
    }

    /**
     * getDenomination method
     * gets the denomination attribute from infoDTO
     *
     * @return  denomination as a String
     */
    public String getDenomination() {
        return denomination;
    }

    /**
     * getDescription method
     * gets the description attribute from infoDTO
     *
     * @return description as a String
     */
    public String getDescription() {
        return description;
    }

    /**
     * Override of equals method
     *
     * @param o Object to be compared to this
     * @return True if this is equal to Object o
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreateAccountForGroupInfoDTO that = (CreateAccountForGroupInfoDTO) o;
        return Objects.equals(accountID, that.accountID) &&
                Objects.equals(denomination, that.denomination) &&
                Objects.equals(description, that.description);
    }

    /**
     * Override of hashCode method
     *
     * @return Integer hashCode
     */
    @Override
    public int hashCode() {
        return Objects.hash(accountID, denomination, description);
    }
}
