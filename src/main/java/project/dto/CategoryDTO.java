package project.dto;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.springframework.hateoas.RepresentationModel;

@Getter
@EqualsAndHashCode(callSuper = false)
public class CategoryDTO extends RepresentationModel<CategoryDTO> {

    private String designation;

    /**
     * Constructor for CategoryDTO
     * @param designation
     */
    public CategoryDTO (String designation){
        this.designation = designation;
    }

    @Override
    public String toString() {
        return "CategoryDTO{" +
                "designation='" + designation + '\'' +
                '}';
    }
}
