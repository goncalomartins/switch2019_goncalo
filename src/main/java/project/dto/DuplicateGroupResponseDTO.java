package project.dto;

import org.springframework.hateoas.RepresentationModel;

import java.util.Collections;
import java.util.Set;

public class DuplicateGroupResponseDTO extends RepresentationModel<DuplicateGroupResponseDTO> {
    private Set<String> members;
    private Set<String> managers;
    private Set<String> categories;
    private String groupID;
    private String ledgerID;
    private String description;
    private String creationDate;

    /**
     * Constructor of GroupDTO class
     *
     * @param members      - List of the identities of all group's person members
     * @param managers     - List of the identities of all group's person managers
     * @param categories   - List of Categories
     * @param groupID      - Identity of the new group as a String
     * @param ledgerID     - Identity of the new group's ledger to be created upon group's creation
     * @param description  - Description of the new group to be created as a String
     * @param creationDate - Creation date of the new group to be created as a String
     */
    public DuplicateGroupResponseDTO(Set<String> members, Set<String> managers, Set<String> categories, String groupID, String ledgerID, String description, String creationDate) {
        setMembers(members);
        setManagers(managers);
        setCategories(categories);
        setGroupID(groupID);
        setLedgerID(ledgerID);
        setDescription(description);
        setCreationDate(creationDate);
    }

    /**
     * getMembers method
     *
     * @return Set of all members' IDs as Strings
     */
    public Set<String> getMembers() {
        return Collections.unmodifiableSet(members);
    }

    /**
     * setMembers method
     * sets the value of GroupDTO's members attribute to contain all members' IDs as Strings
     *
     * @param members Set of Strings of persons' IDs
     */
    private void setMembers(Set<String> members) {
        this.members = Collections.unmodifiableSet(members);
    }

    /**
     * getManagers method
     *
     * @return Set of all managers' IDs as Strings
     */
    public Set<String> getManagers() {
        return Collections.unmodifiableSet(managers);
    }

    /**
     * setManagers method
     * sets the value of GroupDTO's managers attribute to contain all managers' IDs as Strings
     *
     * @param managers Set of Strings of persons' IDs
     */
    private void setManagers(Set<String> managers) {
        this.managers = Collections.unmodifiableSet(managers);
    }

    public Set<String> getCategories() {
        return categories;
    }

    public void setCategories(Set<String> categories) {
        this.categories = categories;
    }

    /**
     * getGroupID method
     *
     * @return the group's ID as a String
     */
    public String getGroupID() {
        return groupID;
    }

    /**
     * setGroupID method
     * sets the value of GroupDTO's groupID attribute to contain the groupID as a String
     *
     * @param groupID
     */
    private void setGroupID(String groupID) {
        this.groupID = groupID;
    }

    /**
     * getLedgerID method
     *
     * @return the group's ledgerID as a String
     */
    public String getLedgerID() {
        return ledgerID;
    }

    /**
     * setLedgerID method
     * sets the value of GroupDTO's ledgerID attribute to contain the ledgerID of the group as a String
     *
     * @param ledgerID
     */
    private void setLedgerID(String ledgerID) {
        this.ledgerID = ledgerID;
    }

    /**
     * getDescription method
     *
     * @return the group's description as a String
     */
    public String getDescription() {
        return description;
    }

    /**
     * setDescription method
     * sets the value of GroupDTO's description attribute to contain the description of the group as a String
     *
     * @param description the group's description as a String
     */
    private void setDescription(String description) {
        this.description = description;
    }

    /**
     * getCreationDate method
     *
     * @return the group's creationDate as a String
     */
    public String getCreationDate() {
        return creationDate;
    }

    /**
     * setCreationDate method
     * sets the value of GroupDTO's creationDate attribute to contain the creationDate of the group as a String
     *
     * @param creationDate the group's creationDate as a String
     */
    private void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

}
