package project.dto;

public class AccountAssembler {

    AccountAssembler() {
        //Constructor is intentionally empty
    }

    public static AccountDTO mapToResponseDTO(String denomination, String description, String accountID) {
        return new AccountDTO(denomination, description, accountID);
    }
}
