package project.dto;

import project.model.entities.group.Group;
import project.model.entities.shared.AccountID;
import project.model.entities.shared.AccountsIDs;

import java.util.HashSet;
import java.util.Set;

public final class CreateAccountForGroupAssembler {

    /**
     * Constructor for CreateAccountForGroupAssembler
     * Created to override the public constructor Java creates automatically if there is not a constructor for a class
     */
    CreateAccountForGroupAssembler() {
        //Constructor is intentionally empty
    }

    /**
     * Method mapToDTO
     * <p>
     * Converts all accounts IDs of a certain group into string
     * and saves it on the Set<String> accountsIDsInString
     *
     * @param group
     * @return accountDTO
     */
    public static CreateAccountForGroupDTO mapToDTO(Group group) {

        // Ir buscar todos os IDs das contas do grupo
        AccountsIDs accountsIDsVO = group.getAccountsIDs();
        Set<AccountID> accountsIDs = accountsIDsVO.getAccountsIDsValue();

        //Set result
        Set<String> accountsIDsInString = new HashSet<>();

        //1- Percorrer o set de id de contas a transformar cada um deles em string
        //2 - Adicionar o id em string ao set accountsIDsInString(result)
        for (AccountID accountID : accountsIDs) {
            accountsIDsInString.add(accountID.toStringDTO());

        }

        //Transformar o atributo do DTO no conteudo do set accountsIDsInString
        return new CreateAccountForGroupDTO(accountsIDsInString);

    }

    /**
     * @param accountID    AccountID of the account to be created
     * @param groupID      GroupID of the group who wants to create the account
     * @param denomination Denomination of the account to be created
     * @param description  Description of the account to be created
     * @return CreateAccountForGroupRequestDTO
     */
    public static CreateAccountForGroupRequestDTO mapToRequestDTO(String accountID, String groupID, String denomination, String description, String personEmail) {
        return new CreateAccountForGroupRequestDTO(accountID, groupID, denomination, description, personEmail);
    }

    public static CreateAccountForGroupResponseDTO mapToResponseDTO(String groupID, String groupDescription, String accountID, String accountDenomination){
        return new CreateAccountForGroupResponseDTO(groupID, groupDescription, accountID, accountDenomination);
    }
}
