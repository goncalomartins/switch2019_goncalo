package project.dto;

import project.model.entities.account.Account;

import java.util.ArrayList;
import java.util.List;

public final class AccountsAssembler {

    /**
     * Constructor for AccountsAssembler
     */
    AccountsAssembler() {
        //Intentionally empty
    }

    /**
     * mapToDTO method
     *
     * @param accounts List of accounts
     * @return AccountsDTO
     */
    public static AccountsDTO mapToDTO(List<Account> accounts) {

        List<AccountDTO> accountsDTOs = new ArrayList<>();

        AccountDTO accountDTO;

        for (Account account : accounts) {

            accountDTO = new AccountDTO(
                    account.getDenomination().toStringDTO(),
                    account.getDescription().toStringDTO(),
                    account.getID().toStringDTO()
            );

            accountsDTOs.add(accountDTO);

        }

        return new AccountsDTO(accountsDTOs);
    }
}
