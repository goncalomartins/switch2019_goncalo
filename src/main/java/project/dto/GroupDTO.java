package project.dto;

import org.springframework.hateoas.RepresentationModel;

import java.util.Collections;
import java.util.Objects;
import java.util.Set;

public class GroupDTO extends RepresentationModel<GroupDTO> {
    private Set<String> members;
    private Set<String> managers;
    private String groupID;
    private String ledgerID;
    private String description;
    private String creationDate;

    /**
     * Constructor of GroupDTO class
     *
     * @param members      - List of the identities of all group's person members
     * @param managers     - List of the identities of all group's person managers
     * @param groupID      - Identity of the new group as a String
     * @param ledgerID     - Identity of the new group's ledger to be created upon group's creation
     * @param description  - Description of the new group to be created as a String
     * @param creationDate - Creation date of the new group to be created as a String
     */
    public GroupDTO(Set<String> members, Set<String> managers, String groupID, String ledgerID, String description, String creationDate) {
        setMembers(members);
        setManagers(managers);
        setGroupID(groupID);
        setLedgerID(ledgerID);
        setDescription(description);
        setCreationDate(creationDate);
    }

    /**
     * getMembers method
     *
     * @return Set of all members' IDs as Strings
     */
    public Set<String> getMembers() {
        return Collections.unmodifiableSet(members);
    }

    /**
     * setMembers method
     * sets the value of GroupDTO's members attribute to contain all members' IDs as Strings
     *
     * @param members Set of Strings of persons' IDs
     */
    private void setMembers(Set<String> members) {
        this.members = Collections.unmodifiableSet(members);
    }

    /**
     * getManagers method
     *
     * @return Set of all managers' IDs as Strings
     */
    public Set<String> getManagers() {
        return Collections.unmodifiableSet(managers);
    }

    /**
     * setManagers method
     * sets the value of GroupDTO's managers attribute to contain all managers' IDs as Strings
     *
     * @param managers Set of Strings of persons' IDs
     */
    private void setManagers(Set<String> managers) {
        this.managers = Collections.unmodifiableSet(managers);
    }

    /**
     * getGroupID method
     *
     * @return the group's ID as a String
     */
    public String getGroupID() {
        return groupID;
    }

    /**
     * setGroupID method
     * sets the value of GroupDTO's groupID attribute to contain the groupID as a String
     *
     * @param groupID
     */
    private void setGroupID(String groupID) {
        this.groupID = groupID;
    }

    /**
     * getLedgerID method
     *
     * @return the group's ledgerID as a String
     */
    public String getLedgerID() {
        return ledgerID;
    }

    /**
     * setLedgerID method
     * sets the value of GroupDTO's ledgerID attribute to contain the ledgerID of the group as a String
     *
     * @param ledgerID
     */
    private void setLedgerID(String ledgerID) {
        this.ledgerID = ledgerID;
    }

    /**
     * getDescription method
     *
     * @return the group's description as a String
     */
    public String getDescription() {
        return description;
    }

    /**
     * setDescription method
     * sets the value of GroupDTO's description attribute to contain the description of the group as a String
     *
     * @param description the group's description as a String
     */
    private void setDescription(String description) {
        this.description = description;
    }

    /**
     * getCreationDate method
     *
     * @return the group's creationDate as a String
     */
    public String getCreationDate() {
        return creationDate;
    }

    /**
     * setCreationDate method
     * sets the value of GroupDTO's creationDate attribute to contain the creationDate of the group as a String
     *
     * @param creationDate the group's creationDate as a String
     */
    private void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * Override of equals method
     *
     * @param o an Object
     * @return True if this Object is equal Object o
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GroupDTO groupDTO = (GroupDTO) o;
        return Objects.equals(members, groupDTO.members) &&
                Objects.equals(managers, groupDTO.managers) &&
                Objects.equals(groupID, groupDTO.groupID) &&
                Objects.equals(ledgerID, groupDTO.ledgerID) &&
                Objects.equals(description, groupDTO.description) &&
                Objects.equals(creationDate, groupDTO.creationDate);
    }

    /**
     * Override of hashCode method
     *
     * @return an integer with the hashCode
     */
    @Override
    public int hashCode() {
        return Objects.hash(members, managers, groupID, ledgerID, description, creationDate);
    }

    @Override
    public String toString() {
        return "GroupDTO{" +
                "members=" + members +
                ", managers=" + managers +
                ", groupID='" + groupID + '\'' +
                ", ledgerID='" + ledgerID + '\'' +
                ", description='" + description + '\'' +
                ", creationDate='" + creationDate + '\'' +
                '}';
    }
}
