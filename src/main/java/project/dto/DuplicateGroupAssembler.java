package project.dto;

import project.model.entities.Categories;
import project.model.entities.group.Group;
import project.model.entities.shared.Category;
import project.model.entities.shared.PersonID;
import project.model.entities.shared.PersonsIDs;

import java.util.HashSet;
import java.util.Set;

public class DuplicateGroupAssembler {

    public static DuplicateGroupRequestDTO mapToRequestDTO(String groupID1, String groupID2, String ledgerID, String personEmail) {
        return new DuplicateGroupRequestDTO(groupID1, groupID2, ledgerID, personEmail);

    }

    public static DuplicateGroupResponseDTO mapToResponseDTO(Group group) {
        Set<String> members = convertPersonsIDs(group.getMembersIDs());
        Set<String> managers = convertPersonsIDs(group.getManagersIDs());
        Set<String> categories = convertCategories(group.getCategories());
        String groupID = group.getID().toStringDTO();
        String groupLedgerID = group.getLedgerID().toStringDTO();
        String description = group.getDescription().getDescriptionValue();
        String creationDate = group.getCreationDate().toStringDTO();

        return new DuplicateGroupResponseDTO(members, managers, categories, groupID, groupLedgerID, description, creationDate);
    }

    private static Set<String> convertPersonsIDs(PersonsIDs personsIDsVO) {
        Set<PersonID> personsIDs = personsIDsVO.getPersonIDs();
        Set<String> personsIDsToString = new HashSet<>();
        for (PersonID personID : personsIDs) {
            personsIDsToString.add(personID.getPersonEmail());
        }
        return personsIDsToString;
    }

    private static Set<String> convertCategories(Categories categories) {
        Set<Category> categoriesValue = categories.getCategoriesValue();
        Set<String> categoriesToString = new HashSet<>();
        for (Category category : categoriesValue) {
            categoriesToString.add(category.toStringDTO());
        }
        return categoriesToString;
    }


}
