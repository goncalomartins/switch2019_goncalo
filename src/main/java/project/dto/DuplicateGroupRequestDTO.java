package project.dto;

import java.util.Objects;

public class DuplicateGroupRequestDTO {

    private String groupID1;
    private String groupID2;
    private String ledgerID;
    private String personEmail;

    public DuplicateGroupRequestDTO(String groupID1, String groupID2, String ledgerID, String personEmail) {
        setGroupID1(groupID1);
        setGroupID2(groupID2);
        setLedgerID(ledgerID);
        setPersonEmail(personEmail);
    }

    public String getGroupID1() {
        return groupID1;
    }

    private void setGroupID1(String groupID1) {
        this.groupID1 = groupID1;
    }

    public String getGroupID2() {
        return groupID2;
    }

    private void setGroupID2(String groupID2) {
        this.groupID2 = groupID2;
    }

    public String getLedgerID() {
        return ledgerID;
    }

    private void setLedgerID(String ledgerID) {
        this.ledgerID = ledgerID;
    }

    public String getPersonEmail() {
        return personEmail;
    }

    private void setPersonEmail(String personEmail) {
        this.personEmail = personEmail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DuplicateGroupRequestDTO that = (DuplicateGroupRequestDTO) o;
        return Objects.equals(groupID1, that.groupID1) &&
                Objects.equals(groupID2, that.groupID2) &&
                Objects.equals(ledgerID, that.ledgerID) &&
                Objects.equals(personEmail, that.personEmail);
    }

    @Override
    public int hashCode() {
        return Objects.hash(groupID1, groupID2, ledgerID, personEmail);
    }
}
