package project.model.entities.account;

import project.exceptions.InvalidFieldException;
import project.frameworkddd.ValueObject;

import java.util.Objects;

public class Denomination implements ValueObject {

    private String denominationValue;

    /**
     * Constructor for Class Denomination
     *
     * @param denomination denomination
     */
    public Denomination(String denomination) {
        setDenominationValue(denomination);
    }

    /**
     * Override equals
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Denomination newDenomination = (Denomination) o;
        return (this.denominationValue.equals(newDenomination.denominationValue));
    }

    /**
     * Override hashCode
     */
    @Override
    public int hashCode() {
        return Objects.hash(this.denominationValue);
    }

    /**
     * Override toString
     */
    @Override
    public String toString() {
        return "Denomination{" +
                "denomination='" + denominationValue + '\'' +
                '}';
    }

    public String getDenominationValue() {
        return denominationValue;
    }

    /**
     * Create attribute denomination
     *
     * @param denominationValue Denomination of account
     */
    private void setDenominationValue(String denominationValue) {
        if (denominationValue != null && !denominationValue.isEmpty() && !denominationValue.trim().isEmpty()) {
            this.denominationValue = denominationValue;
        } else {
            throw new InvalidFieldException("Input 'denomination' is invalid!");
        }
    }

    public String toStringDTO() {
        return denominationValue;
    }
}
