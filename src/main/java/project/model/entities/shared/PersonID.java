package project.model.entities.shared;

import lombok.NoArgsConstructor;
import project.exceptions.InvalidFieldException;
import project.frameworkddd.ValueObject;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@NoArgsConstructor
@Embeddable
public class PersonID implements ValueObject, Serializable {

    private String personEmail;

    /**
     * Constructor for PersonID
     *
     * @param personID unique identifier number of PersonID
     */
    public PersonID(String personID) {
        setPersonEmail(personID);
    }

    /**
     * Compares internal attribute id of two PersonID objects
     * Condition to be verified in equals() override
     *
     * @param other
     * @return
     */
    public boolean sameValueAs(PersonID other) {
        return other != null && this.personEmail.equals(other.personEmail);
    }

    /**
     * Get attribute id
     *
     * @return id attribute
     */
    public String getPersonEmail() {
        return personEmail;
    }


    /**
     * Method setPersonEmail
     *
     * @param personEmail
     */
    public void setPersonEmail(String personEmail) {
        if (isValidEmailAddress(personEmail)) {
            this.personEmail = personEmail;
        } else {
            throw new InvalidFieldException();
        }
    }

    /**
     * Method to control if the input personEmail is valid
     *
     * @param email
     * @return
     */
    public boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1," +
                "3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PersonID)) {
            return false;
        }
        PersonID other = (PersonID) o;
        return sameValueAs(other);
    }

    @Override
    public int hashCode() {
        return Objects.hash(personEmail);
    }

    @Override
    public String toString() {
        return "PersonID{" +
                "personID=" + personEmail +
                '}';
    }

    /**
     * toStringDTO
     */
    public String toStringDTO() {
        return personEmail;
    }
}
