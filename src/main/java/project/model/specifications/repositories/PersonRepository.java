package project.model.specifications.repositories;

import project.model.entities.person.Person;
import project.model.entities.shared.PersonID;

import java.util.Optional;

/**
 * Interface PersonRepository
 */
public interface PersonRepository {

    Person save(final Person object);

    Optional<Person> findById(PersonID personID);

}
