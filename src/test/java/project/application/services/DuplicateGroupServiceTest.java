package project.application.services;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.DuplicateGroupRequestDTO;
import project.dto.DuplicateGroupResponseDTO;
import project.frameworkddd.IUSDuplicateGroupService;
import project.model.entities.group.Group;
import project.model.entities.group.GroupID;
import project.model.entities.shared.LedgerID;
import project.model.entities.shared.PersonID;
import project.model.specifications.repositories.GroupRepository;
import project.model.specifications.repositories.LedgerRepository;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("serviceTest")
@ExtendWith(SpringExtension.class)
class DuplicateGroupServiceTest {

    @Autowired
    GroupRepository groupRepo;
    @Autowired
    LedgerRepository ledgerRepo;
    @Autowired
    IUSDuplicateGroupService service;

    @Disabled
    @Test
    void duplicateGroupHappyCaseTest() {
        DuplicateGroupRequestDTO requestDTO = new DuplicateGroupRequestDTO("510200", "99999", "11", "51010@switch.pt");

        Set<String> expectedMembers = new HashSet<>();
        expectedMembers.add("51010@switch.pt");

        Set<String> expectedManagers = new HashSet<>();
        expectedManagers.add("51010@switch.pt");

        Set<String> expectedCategories = new HashSet<>();
        expectedCategories.add("Arbitros");

        String expectedGroupID = "510200";
        String expectedLedgerID = "11";
        String expectedDescription = "Futebol";
        String expectedCreationDate = "2020-07-06";

        Group mockedGroup = Mockito.mock(Group.class);

        Mockito.when(groupRepo.findById(new GroupID("510200"))).thenReturn(Optional.of(mockedGroup));
        Mockito.when((mockedGroup.hasMemberID(new PersonID("51010@switch.pt")))).thenReturn(true);

        Mockito.when(groupRepo.findById(new GroupID("99999"))).thenReturn(Optional.empty());
        Mockito.when(ledgerRepo.findById(new LedgerID("11"))).thenReturn(Optional.empty());


        DuplicateGroupResponseDTO expected = new DuplicateGroupResponseDTO(expectedMembers, expectedManagers, expectedCategories, expectedGroupID, expectedLedgerID, expectedDescription, expectedCreationDate);

        DuplicateGroupResponseDTO result = service.duplicateGroup(requestDTO);

        assertEquals(expected, result);

    }


}