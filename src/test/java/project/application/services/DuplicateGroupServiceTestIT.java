package project.application.services;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import project.ProjectApplication;
import project.dto.DuplicateGroupRequestDTO;
import project.dto.DuplicateGroupResponseDTO;
import project.frameworkddd.IUSDuplicateGroupService;
import project.model.specifications.repositories.GroupRepository;
import project.model.specifications.repositories.LedgerRepository;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Transactional
@SpringBootTest(classes = ProjectApplication.class)
class DuplicateGroupServiceTestIT {

    @Autowired
    GroupRepository groupRepo;
    @Autowired
    LedgerRepository ledgerRepo;
    @Autowired
    IUSDuplicateGroupService service;

    @Test
    void duplicateGroupHappyCaseTest() {
        DuplicateGroupRequestDTO requestDTO = new DuplicateGroupRequestDTO("510200", "99999", "11", "51010@switch.pt");

        Set<String> expectedMembers = new HashSet<>();
        expectedMembers.add("51010@switch.pt");

        Set<String> expectedManagers = new HashSet<>();
        expectedManagers.add("51010@switch.pt");

        Set<String> expectedCategories = new HashSet<>();
        expectedCategories.add("Arbitros");

        String expectedGroupID = "510200";
        String expectedLedgerID = "11";
        String expectedDescription = "Futebol";
        String expectedCreationDate = "2020-07-06";

        DuplicateGroupResponseDTO expected = new DuplicateGroupResponseDTO(expectedMembers, expectedManagers, expectedCategories, expectedGroupID, expectedLedgerID, expectedDescription, expectedCreationDate);

        DuplicateGroupResponseDTO result = service.duplicateGroup(requestDTO);

        assertEquals(expected, result);

    }


}