package project.model.entities.ledger;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.exceptions.InvalidFieldException;
import project.exceptions.TransactionAlreadyExistsException;
import project.model.entities.account.Denomination;
import project.model.entities.shared.AccountID;
import project.model.entities.shared.Category;
import project.model.entities.shared.LedgerID;
import project.model.entities.shared.Period;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class LedgerTest {

    private AccountID smithsGroceriesAccountID;
    private AccountID zeCreditAccountID;
    private Category newCategory;
    private LedgerID newLedgerID;
    private Ledger newLedger;


    @BeforeEach
    public void init() {
        smithsGroceriesAccountID = new AccountID("11111");
        zeCreditAccountID = new AccountID("22222");
        newCategory = new Category("Groceries");
        newLedgerID = new LedgerID("33333");
        newLedger = new Ledger(newLedgerID);
    }

    /**
     * Test for constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for Ledger constructor - HappyCase")
    void LedgerConstructorHappyCaseTest() {
        assertTrue(newLedger instanceof Ledger);
    }

    /**
     * Test for equals
     * Happy case
     */
    @Test
    @DisplayName("Test forEquals - Same denomination & description")
    void equalsHappyCaseTest() {
        //Arrange
        //Act
        boolean result = newLedger.equals(newLedger);
        //Assert
        assertTrue(result);
    }

    /**
     * Test for equals
     * False case
     */
    @Test
    @DisplayName("Test forEquals - different IDs")
    void equalsFalseCaseTest() {
        //Arrange
        LedgerID secondLedgerID = new LedgerID("44444");
        Ledger secondLedger = new Ledger(secondLedgerID);
        //Act
        boolean result = newLedger.equals(secondLedger);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure false with null object
     */
    @Test
    @DisplayName("Test forEquals - Comparing two instances of Account being one of them null")
    void equalsEnsureFalseWithNullTest() {
        //Arrange
        Ledger mainB = null;
        //Act
        boolean result = newLedger.equals(mainB);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure two objects from different classes are different
     */
    @Test
    @DisplayName("Test forEquals - Confirm that two objects from different classes are not the same")
    void equalsObjectsFromDifferentClasses() {
        //Arrange
        //Act
        boolean result = newLedger.equals(newCategory);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for getAccountID
     * Ensure works
     */
    @Test
    @DisplayName("Test for getAccountID - Ensure works")
    void getAccountIDEnsureTrueTest() {
        //arrange
        LedgerID expected = newLedgerID;

        //act
        LedgerID result = newLedger.getID();

        //assert
        assertEquals(expected, result);
    }

    /**
     * Test for constructor
     * Exception with null LedgerID
     */
    @Test
    @DisplayName("Test for constructor - Ensure exception with null LedgerID")
    void constructorEnsureExceptionWithNullLedgerIDTest() {
        assertThrows(InvalidFieldException.class, () -> {
            Ledger thisLedger = new Ledger(null);
        });
    }

    /**
     * Test for addTransaction
     * Happy case
     */
    @Test
    @DisplayName("Test for addTransaction - HappyCase")
    void addTransactionToLedgerHappyCase() {
        // ARRANGE
        int initialTransactionsSize = newLedger.getTransactions().size();

        // ACT
        newLedger.addTransaction("49.99", Type.DEBIT, "2020-01-13 00:00:00", "Groceries for January", newCategory,
                smithsGroceriesAccountID, zeCreditAccountID);

        // ASSERT
        assertEquals(initialTransactionsSize + 1, newLedger.getTransactions().size());
    }

    /**
     * Test for addTransaction
     * Same Transaction
     */
    @Test
    @DisplayName("Test for addTransaction - Same transaction")
    void addTransactionToLedgerSameTransactionTest() {
        // ARRANGE
        newLedger.addTransaction("49.99", Type.DEBIT, "2020-01-13 00:00:00", "Groceries for January", newCategory,
                smithsGroceriesAccountID, zeCreditAccountID);

        // ACT
        // ASSERT
        assertThrows(TransactionAlreadyExistsException.class, () -> {
            newLedger.addTransaction("49.99", Type.DEBIT, "2020-01-13 00:00:00", "Groceries for January", newCategory
                    , smithsGroceriesAccountID, zeCreditAccountID);
        });
    }

    /**
     * Test for addTransaction
     * Throwing exception - Null Description attribute
     */
    @Test
    @DisplayName("Test for addTransaction - Verify if exception is thrown - description cant be null ")
    void addTransactionVerifyIfThrowsExceptionWithNullString() {
        //ARRANGE

        //ASSERT
        assertThrows(InvalidFieldException.class, () -> {
            newLedger.addTransaction("49.99", Type.DEBIT, "2020-01-13 00:00:00", null, newCategory,
                    smithsGroceriesAccountID, zeCreditAccountID);
        });
    }

    /**
     * Test for getBalanceWithinPeriod
     * Ensure 0 if no transactions
     */
    @Test
    @DisplayName("Test for getBalanceWithinPeriod - Ensure 0 if no transactions")
    void getBalanceWithinPeriodEnsure0IfNoTransactions() {
        //ARRANGE
        Period period = new Period("2019-01-01 00:00:00", "2020-01-01 00:00:00");
        double expected = 0;

        //ACT
        double actual = newLedger.getBalanceWithinPeriod(period);

        //ASSERT
        assertEquals(expected, actual);
    }

    /**
     * Test for getBalanceWithinPeriod
     * Ensure correct balance with all transactions within timeframe
     */
    @Test
    @DisplayName("Test for getBalanceWithinPeriod - Ensure correct balance with all transactions within timeframe")
    void getBalanceWithinPeriodEnsureCorrectBalanceWithAllTransactionsWithinTimeframeTest() {
        //ARRANGE
        newLedger.addTransaction("20", Type.DEBIT, "2020-01-01 00:00:00", "Bags for plastic", newCategory,
                smithsGroceriesAccountID, zeCreditAccountID);
        newLedger.addTransaction("100", Type.CREDIT, "2020-01-10 00:00:00", "Selling of plastic", newCategory,
                smithsGroceriesAccountID, zeCreditAccountID);
        Period period = new Period("2019-01-01 00:00:00", "2020-12-31 00:00:00");
        double expected = 80;

        // ACT
        double actual = newLedger.getBalanceWithinPeriod(period);

        //ASSERT
        assertEquals(expected, actual);
    }

    /**
     * Test for getBalanceWithinPeriod
     * Ensure correct balance with some transactions within timeframe
     */
    @Test
    @DisplayName("Test for getBalanceWithinPeriod - Ensure correct balance with some transactions within timeframe")
    void getBalanceWithinPeriodEnsureCorrectBalanceWithSomeTransactionsWithinTimeframeTest() {
        //ARRANGE
        newLedger.addTransaction("20", Type.DEBIT, "2020-01-01 00:00:00", "Bags for plastic", newCategory,
                smithsGroceriesAccountID, zeCreditAccountID);
        newLedger.addTransaction("100", Type.CREDIT, "2020-01-10 00:00:00", "Selling of plastic", newCategory,
                smithsGroceriesAccountID, zeCreditAccountID);
        Period period = new Period("2020-01-05 00:00:00", "2020-01-20 00:00:00");
        double expected = 100;

        // ACT
        double actual = newLedger.getBalanceWithinPeriod(period);

        //ASSERT
        assertEquals(expected, actual);
    }

    /**
     * Test for getBalanceWithinPeriod
     * Ensure correct balance with no transactions within timeframe
     */
    @Test
    @DisplayName("Test for getBalanceWithinPeriod - Ensure correct balance with no transactions within timeframe")
    void getBalanceWithinPeriodEnsureCorrectBalanceWithNoTransactionsWithinTimeframeTest() {
        //ARRANGE
        newLedger.addTransaction("20", Type.DEBIT, "2020-01-01 00:00:00", "Bags for plastic", newCategory,
                smithsGroceriesAccountID, zeCreditAccountID);
        newLedger.addTransaction("100", Type.CREDIT, "2020-01-10 00:00:00", "Selling of plastic", newCategory,
                smithsGroceriesAccountID, zeCreditAccountID);
        Period period = new Period("2019-01-01 00:00:00", "2019-12-31 00:00:00");
        double expected = 0;

        // ACT
        double actual = newLedger.getBalanceWithinPeriod(period);

        //ASSERT
        assertEquals(expected, actual);

    }

    /**
     * Test for getTransactionsWithinPeriod
     * Happy Case
     */
    @Test
    @DisplayName("Test for getTransactionsWithinPeriod - Happy Case")
    void getTransactionsWithinPeriodHappyCaseTest() {
        //ARRANGE
        newLedger.addTransaction("100", Type.DEBIT, "2019-11-01 00:00:00", "Despesas", newCategory,
                smithsGroceriesAccountID, zeCreditAccountID);
        newLedger.addTransaction("200", Type.CREDIT, "2019-12-30 00:00:00", "Despesas", newCategory,
                smithsGroceriesAccountID, zeCreditAccountID);
        newLedger.addTransaction("300", Type.CREDIT, "2020-01-01 00:00:00", "Despesas", newCategory,
                smithsGroceriesAccountID, zeCreditAccountID);

        Period period = new Period("2019-01-01 00:00:00", "2019-12-31 00:00:00");

        Set<Transaction> expected = new HashSet<>();
        Transaction transaction1 = new Transaction("100", Type.DEBIT, "2019-11-01 00:00:00", "Despesas", newCategory,
                smithsGroceriesAccountID, zeCreditAccountID);
        Transaction transaction2 = new Transaction("200", Type.CREDIT, "2019-12-30 00:00:00", "Despesas", newCategory
                , smithsGroceriesAccountID, zeCreditAccountID);
        expected.add(transaction1);
        expected.add(transaction2);

        //ACT
        Set<Transaction> actual = newLedger.getTransactionsWithinPeriod(period);

        //ASSERT
        assertEquals(expected, actual);
    }

    /**
     * Test for getTransactionsWithinPeriod
     * Test all transactions outside the period, expecting empty hashSet result
     */
    @Test
    @DisplayName("Test for getTransactionsWithinPeriod - Empty result")
    void getTransactionsWithinPeriodEmptyResultTest() {
        //ARRANGE
        String dateI = "2019-01-01 00:00:00";
        String dateF = "2019-12-31 00:00:00";
        Period emptyPeriod = new Period(dateI, dateF);

        newLedger.addTransaction("100", Type.DEBIT, "2020-11-01 00:00:00", "Despesas", newCategory,
                smithsGroceriesAccountID, zeCreditAccountID);
        newLedger.addTransaction("200", Type.CREDIT, "2018-12-30 00:00:00", "Despesas", newCategory,
                smithsGroceriesAccountID, zeCreditAccountID);
        newLedger.addTransaction("300", Type.CREDIT, "2020-01-01 00:00:00", "Despesas", newCategory,
                smithsGroceriesAccountID, zeCreditAccountID);

        Set<Transaction> expected = new HashSet<>();

        //ACT
        Set<Transaction> actual = newLedger.getTransactionsWithinPeriod(emptyPeriod);

        //ASSERT
        assertEquals(expected, actual);
    }

    /**
     * Test for getTransactionsOfAccountWithinPeriod
     * Happy case
     */
    @Test
    @DisplayName("Test for getTransactionsOfAccountWithinPeriod - Happy case")
    void getTransactionsOfAccountWithinPeriodHappyCaseTest() {
        //ARRANGE
        String initialDate = "2019-01-01 00:00:00";
        String finalDate = "2019-12-31 00:00:00";
        Period period = new Period(initialDate, finalDate);

        newLedger.addTransaction("100", Type.DEBIT, "2019-11-01 00:00:00", "Despesas", newCategory,
                smithsGroceriesAccountID, zeCreditAccountID);
        newLedger.addTransaction("200", Type.CREDIT, "2018-12-30 00:00:00", "Despesas", newCategory,
                smithsGroceriesAccountID, zeCreditAccountID);
        newLedger.addTransaction("300", Type.CREDIT, "2020-01-01 00:00:00", "Despesas", newCategory,
                smithsGroceriesAccountID, zeCreditAccountID);

        Transaction newTransaction = new Transaction("100", Type.DEBIT, "2019-11-01 00:00:00", "Despesas",
                newCategory, smithsGroceriesAccountID, zeCreditAccountID);
        Set<Transaction> expected = new HashSet<>();
        expected.add(newTransaction);

        //ACT
        Set<Transaction> actual = newLedger.getTransactionsOfAccountWithinPeriod(smithsGroceriesAccountID, period);

        //ASSERT
        assertEquals(expected, actual);
    }

    /**
     * Test for getTransactionsOfAccountWithinPeriod
     * Ensure the transaction isn't returned if the account to be searched (by parameter) is not related to the
     * transaction
     */
    @Test
    @DisplayName("Test for getTransactionsOfAccountWithinPeriod - Happy case")
    void getTransactionsOfAccountWithinPeriodEnsureNotRelatedTransactionIsReturnedTest() {
        //ARRANGE
        String initialDate = "1900-01-01 00:00:00";
        String finalDate = "2100-12-31 00:00:00";
        Period period = new Period(initialDate, finalDate);

        AccountID differentAccountId = new AccountID("1121654465");

        newLedger.addTransaction("100", Type.DEBIT, "2019-11-01 00:00:00", "Despesas", newCategory,
                smithsGroceriesAccountID, zeCreditAccountID);
        newLedger.addTransaction("200", Type.CREDIT, "2018-12-30 00:00:00", "Despesas", newCategory,
                smithsGroceriesAccountID, zeCreditAccountID);
        newLedger.addTransaction("300", Type.CREDIT, "2020-01-01 00:00:00", "Despesas", newCategory,
                differentAccountId, zeCreditAccountID);

        Transaction newTransaction = new Transaction("100", Type.DEBIT, "2019-11-01 00:00:00", "Despesas",
                newCategory, smithsGroceriesAccountID, zeCreditAccountID);
        Transaction newTransaction2 = new Transaction("200", Type.CREDIT, "2018-12-30 00:00:00", "Despesas",
                newCategory, smithsGroceriesAccountID, zeCreditAccountID);
        Set<Transaction> expected = new HashSet<>();
        expected.add(newTransaction);
        expected.add(newTransaction2);

        //ACT
        Set<Transaction> actual = newLedger.getTransactionsOfAccountWithinPeriod(smithsGroceriesAccountID, period);

        //ASSERT
        assertEquals(expected, actual);
    }

    /**
     * Test for getTransactionsOfAccountWithinPeriod
     * Test all transactions outside the period, expecting empty hashSet result
     */
    @Test
    @DisplayName("Test for getTransactionsOfAccountWithinPeriod - Empty result")
    void getTransactionsOfAccountWithinPeriodEmptyResultTest() {
        //ARRANGE
        String initialDate = "2019-01-01 00:00:00";
        String finalDate = "2019-12-31 00:00:00";
        Period period = new Period(initialDate, finalDate);

        newLedger.addTransaction("100", Type.DEBIT, "2020-11-01 00:00:00", "Despesas", newCategory,
                smithsGroceriesAccountID, zeCreditAccountID);
        newLedger.addTransaction("200", Type.CREDIT, "2018-12-30 00:00:00", "Despesas", newCategory,
                smithsGroceriesAccountID, zeCreditAccountID);
        newLedger.addTransaction("300", Type.CREDIT, "2020-01-01 00:00:00", "Despesas", newCategory,
                smithsGroceriesAccountID, zeCreditAccountID);

        Set<Transaction> expected = new HashSet<>();

        //ACT
        Set<Transaction> actual = newLedger.getTransactionsOfAccountWithinPeriod(smithsGroceriesAccountID, period);

        //ASSERT
        assertEquals(expected, actual);
    }

    /**
     * Test for toString
     * Ensure true
     */
    @Test
    @DisplayName("Test for toString - Ensure true")
    void toStringEnsureTrueTest() {
        // ARRANGE
        newLedger.addTransaction("10", Type.CREDIT, "2020-01-13 00:00:00", "Groceries for January", newCategory,
                zeCreditAccountID, smithsGroceriesAccountID);
        newLedger.addTransaction("50", Type.DEBIT, "2020-01-13 00:00:00", "Groceries for January", newCategory,
                smithsGroceriesAccountID, zeCreditAccountID);
        String expected = "Ledger{ledgerID=LedgerID{ledgerID=33333}, transactions=Transactions{transactions=[" +
                "Transaction{amount=Amount{amount=10.0}, dateTime=DateTime{dateTime='2020-01-13T00:00'}, " +
                "type=Type{type=1}, description=Description{description='Groceries for January'}, " +
                "category=Category{designation=Designation{designation='Groceries'}}, " +
                "debitAccountID=AccountID{accountID=22222}, creditAccountID=AccountID{accountID=11111}}, " +
                "Transaction{amount=Amount{amount=50.0}, dateTime=DateTime{dateTime='2020-01-13T00:00'}, " +
                "type=Type{type=-1}, description=Description{description='Groceries for January'}, " +
                "category=Category{designation=Designation{designation='Groceries'}}, " +
                "debitAccountID=AccountID{accountID=11111}, creditAccountID=AccountID{accountID=22222}}]}}";
        // ACT
        String actual = newLedger.toString();

        // ASSERT
        assertEquals(expected, actual);
    }

    /**
     * Test for Ledger hashcode
     * Happy case
     */
    @Test
    @DisplayName("Test for hashcode - Ensure true with same attributes")
    void TransactionHashCodeEnsureTrueTest() {
        //Arrange
        Ledger newLedger = new Ledger(newLedgerID);
        int expectedHash = 33395;

        //Act
        int actualHash = newLedger.hashCode();

        //Assert
        assertEquals(expectedHash, actualHash);
    }

    /**
     * Test false for sameAs Method
     */
    @Test
    @DisplayName("Test false for sameAs Method in Ledger")
    void ledgerSameAsFalse() {
        //Arrange
        Ledger newLedger = new Ledger(newLedgerID);
        Denomination newDenomination = new Denomination("Conta Principal");

        //Act
        boolean result = newLedger.sameAs(newDenomination);

        //Assert
        assertFalse(result);
    }
}