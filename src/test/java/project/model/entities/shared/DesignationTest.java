package project.model.entities.shared;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.exceptions.InvalidFieldException;

import static org.junit.jupiter.api.Assertions.*;

public class DesignationTest {

    /**
     * Test for Designation Constructor
     * Happy Case
     */
    @Test
    @DisplayName("Test for Designation Constructor - Happy Case")
    void DesignationConstructorHappyCase() {
        //ARRANGE
        Designation cd1 = new Designation("medication");

        //ASSERT
        assertTrue(cd1 instanceof Designation);

    }

    /**
     * Test for Designation Constructor
     * Ensure exception is thrown - Designation can't be empty string
     */
    @Test
    @DisplayName("Test for Designation Constructor - Empty Designation")
    void DesignationConstructorEnsureThrowsExceptionWithEmptyStringTest() {

        //ASSERT
        assertThrows(InvalidFieldException.class, () -> {
            Designation cd1 = new Designation("");
        });
    }

    /**
     * Test for Designation Constructor
     * Ensure exception is thrown - Designation can't be null
     */
    @Test
    @DisplayName("Test for Designation Constructor - Null Designation")
    void DesignationConstructorNullDesignation() {

        //ASSERT
        assertThrows(InvalidFieldException.class, () -> {
            Designation cd1 = new Designation(null);
        });
    }

    /**
     * Test for Designation Constructor
     * Ensure exception is thrown - Designation can't be Empty
     */
    @Test
    @DisplayName("Test for Designation Constructor - Empty ")
    void DesignationConstructorEnsureFalseWithOnlySpacesStringTest() {

        //ASSERT
        assertThrows(InvalidFieldException.class, () -> {
            Designation cd1 = new Designation("     ");
        });

    }

    /**
     * Test for  getDesignationValue method
     * Happy Case
     */
    @Test
    @DisplayName("Test for getDesignationValue method - Happy Case ")
    void getDesignationValueTest() {
        //ARRANGE
        Designation designation = new Designation("Motas");
        String expected = "Motas";
        //ACT
        String result = designation.getDesignationValue();
        //ASSERT
        assertEquals(expected, result);

    }

    /**
     * Test for  getDesignationValue method
     * Ensure not equals
     */
    @Test
    @DisplayName("Test for getDesignationValue method - Ensure not equals ")
    void getDesignationValueEnsureNotEqualsTest() {
        //ARRANGE
        Designation designation = new Designation("Motas");
        String expected = "Carros";
        //ACT
        String result = designation.getDesignationValue();
        //ASSERT
        assertNotEquals(expected, result);

    }


    /**
     * Test for Designation toString
     * HappyCase
     */
    @Test
    @DisplayName("Test for Designation toString - HappyCase")
    void DesignationToStringHappyCaseTest() {
        //Arrange
        Designation newDesignation = new Designation("Comida");
        String expected = "Designation{designation='Comida'}";
        //Act
        String actual = newDesignation.toString();
        //Assert
        assertEquals(expected, actual);
    }

    /**
     * Test for Designation equals
     * Ensure true with same object
     */
    @Test
    @DisplayName("Test for equals - Ensure true with same object")
    void DesignationEqualsTrueWithSameObjectTest() {

        //arrange
        Designation c1 = new Designation("groceries");

        //assert
        assertTrue(c1.equals(c1));
    }

    /**
     * Test for Designation equals
     * Ensure true with same attribute
     */
    @Test
    @DisplayName("Test for equals - Ensure true with same attributes")
    void DesignationEqualsTrueWithSameAttributesTest() {

        //arrange
        Designation c1 = new Designation("groceries");
        Designation c2 = new Designation("groceries");

        //assert
        assertTrue(c1.equals(c2));
    }

    /**
     * Test for Designation equals
     * Ensure true with same attribute
     */
    @Test
    @DisplayName("Test for equals - Ensure false with different attributes")
    void DesignationEqualsFalseWithDifferentAttributesTest() {

        //arrange
        Designation c1 = new Designation("groceries");
        Designation c2 = new Designation("shopping");

        //assert
        assertFalse(c1.equals(c2));
    }

    /**
     * Test for Designation equals
     * Ensure false - Comparing two objects from different classes
     */
    @Test
    @DisplayName("Test for equals - Objects from different classes")
    void DesignationEqualsFalseComparingTwoObjectsFromDifferentClasses() {

        //arrange
        Category c1 = new Category("groceries");
        Designation c2 = new Designation("shopping");

        //assert
        assertFalse(c2.equals(c1));
    }

    /**
     * Test for Designation equals
     * Ensure false- comparing two objects being one of them null
     */
    @Test
    @DisplayName("Test for equals - Ensure false since one of the objects is null")
    void DesignationEqualsFalseTwoObjectsBeingOfOfThemNullTest() {
        Designation c1 = null;
        Designation c2 = new Designation("shopping");

        assertFalse(c2.equals(c1));

    }

    /**
     * Test for Designation hashcode
     * Happy Case
     */
    @Test
    @DisplayName("Test for hashcode - Happy Case")
    void DesignationHashcodeEnsureTrueTest() {
        Designation health = new Designation("Health");
        int expectedHash = -2137395557;
        int actualHash = health.hashCode();
        assertEquals(expectedHash, actualHash);
    }

    /**
     * Test for Designation hashcode
     * Ensure not equals
     */
    @Test
    @DisplayName("Test for hashcode - Ensure Not Equals")
    void DesignationHashCodeEnsureNotEqualsTest() {
        Designation health = new Designation("Health");
        int expectedHash = -56156456;
        int actualHash = health.hashCode();
        assertNotEquals(expectedHash, actualHash);
    }

}
