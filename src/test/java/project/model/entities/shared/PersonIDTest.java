package project.model.entities.shared;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.exceptions.InvalidFieldException;

import static org.junit.jupiter.api.Assertions.*;

class PersonIDTest {

    /**
     * Test for PersonID Constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for PersonID constructor - Happy case")
    void PersonIDConstructorHappyCaseTest() {
        PersonID one = new PersonID("1@switch.pt");
        assertTrue(one instanceof PersonID);
    }

    /**
     * Test for sameValueAs
     */
    @Test
    @DisplayName("Test for PersonID sameValueAs - True case")
    void PersonIDSameValueAsTrueTest() {
        //Arrange
        String one = "1@switch.pt";
        PersonID a = new PersonID(one);
        String two = "1@switch.pt";
        PersonID b = new PersonID(two);
        //Act
        boolean result = a.sameValueAs(b);
        //Assert
        assertTrue(result);
    }

    /**
     * Test for sameValueAs
     */
    @Test
    @DisplayName("Test for PersonID sameValueAs - False case")
    void PersonIDSameValueAsFalseTest() {
        //Arrange
        String one = "1@switch.pt";
        PersonID a = new PersonID(one);
        String two = "2@switch.pt";
        PersonID b = new PersonID(two);
        //Act
        boolean result = a.sameValueAs(b);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for sameValueAs
     * Ensure false with null
     */
    @Test
    @DisplayName("Test for PersonID sameValueAs - False case")
    void sameValueAsEnsureFalseWithNullTest() {
        //Arrange
        PersonID a = new PersonID("1@switch.pt");
        //Act
        boolean result = a.sameValueAs(null);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for setPersonEmail method
     * Happy Case
     */
    @Test
    void setPersonEmailTestHappyCaseTest() {
        //Arrange
        PersonID personID = new PersonID("1@switch.pt");
        String expected = "new_email@switch.pt";

        //Act
        personID.setPersonEmail("new_email@switch.pt");
        String actual = personID.getPersonEmail();

        //Assert
        assertEquals(expected, actual);

    }

    /**
     * Test for setPersonEmail method
     * Ensure throws exception if email is invalid
     */
    @Test
    void setPersonEmailEnsureThrowsExceptionIfEmailIsInvalidTest() {
        //Arrange
        PersonID personID = new PersonID("1@switch.pt");

        //Act
        //Assert
        Assertions.assertThrows(InvalidFieldException.class, () -> {
            personID.setPersonEmail("1switch.pt");
        });

    }

    /**
     * Test for equals - Happy Case
     */
    @Test
    @DisplayName("Test for equals - Happy Case")
    void equalsHappyCaseTest() {
        //Arrange
        PersonID ana = new PersonID("1@switch.pt");
        PersonID susana = new PersonID("1@switch.pt");
        //Act
        Boolean result = ana.equals(susana);
        //Assert
        assertTrue(result);
    }

    /**
     * Test for equals - Not equals case
     */
    @Test
    @DisplayName("Test for equals - Not Equals Case")
    void notEqualsHappyCaseTest() {
        //Arrange
        PersonID ana = new PersonID("1@switch.pt");
        PersonID susana = new PersonID("2@switch.pt");
        //Act
        Boolean result = ana.equals(susana);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals - Null object
     */
    @Test
    @DisplayName("Test for equals - Null object")
    void equalsNotNullTest() {
        //Arrange
        PersonID ana = new PersonID("1@switch.pt");
        PersonID susana = null;
        //Act
        Boolean result = ana.equals(susana);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals - objects different classes
     */
    @Test
    @DisplayName("Test for equals - objects different classes")
    void equalsDifferentClassesTest() {
        //Arrange
        PersonID ana = new PersonID("1@switch.pt");
        AccountID food = new AccountID("1");
        //Act
        Boolean result = ana.equals(food);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for hash code - happy case
     */
    @Test
    @DisplayName("test for Hash code - Happy case")
    void hashCodeHappyCaseTest() {
        //Arrange
        PersonID ana = new PersonID("1@switch.pt");
        int expectedHash = -191904306;
        //Act
        int actualHash = ana.hashCode();
        //Assert
        assertEquals(expectedHash, actualHash);
    }

    /**
     * Test for hash code - not equals case
     */
    @Test
    @DisplayName("test for Hash code - not equals case")
    void hashCodeNotEqualsTest() {
        //Arrange
        PersonID ana = new PersonID("1@switch.pt");
        int expectedHash = 33395;
        //Act
        int actualHash = ana.hashCode();
        //Assert
        assertNotEquals(expectedHash, actualHash);
    }

    /**
     * Test for toString - Happy Case
     */
    @Test
    @DisplayName("test for toString - Happy Case")
    void toStringHappyCaseTest() {
        //Arrange
        PersonID ana = new PersonID("1@switch.pt");
        String expected = "PersonID{personID=1@switch.pt}";
        //Act
        String result = ana.toString();
        //Assert
        assertEquals(expected, result);
    }

    /**
     * Test for toString - not equals case
     */
    @Test
    @DisplayName("test for toString - not equals case")
    void toStringNotEqualsTest() {
        //Arrange
        PersonID ana = new PersonID("1@switch.pt");
        String expected = "";
        //Act
        String result = ana.toString();
        //Assert
        assertNotEquals(expected, result);
    }

    /**
     * Test for getId - Happy Case
     */
    @Test
    @DisplayName("test for getId")
    void getIdHappyCaseTest() {
        //ARRANGE
        PersonID a = new PersonID("1@switch.pt");
        String expected = "1@switch.pt";
        //ACT
        String result = a.getPersonEmail();
        //ASSERT
        assertEquals(expected, result);
    }
}