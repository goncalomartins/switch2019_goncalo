package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class TransactionDTOTest {

    TransactionDTO transactionDTO;

    @BeforeEach
    void init() {
        transactionDTO = new TransactionDTO("1000", "Debit", "1991-22-12 00:00:00", "diapers", "baby stuff", "1", "2");
    }

    /**
     * Test for TransactionDTO Constructor
     * Necessary to ensure full coverage
     */
    @Test
    void constructorTest() {
        //ARRANGE
        //ACT
        //ASSERT
        assertTrue(transactionDTO instanceof TransactionDTO);
    }

    /**
     * Test for getAmount method
     * Happy Case
     */
    @Test
    void getAmountHappyCaseTest() {
        //ARRANGE
        String expected = "1000";

        //ACT
        String actual = transactionDTO.getAmount();

        //ASSERT
        assertEquals(expected, actual);

    }

    /**
     * Test for getType method
     * Happy Case
     */
    @Test
    void getTypeHappyCaseTest() {
        //ARRANGE
        String expected = "Debit";

        //ACT
        String actual = transactionDTO.getType();

        //ASSERT
        assertEquals(expected, actual);

    }

    /**
     * Test for getDateTime method
     * Happy Case
     */
    @Test
    void getDateTimeHappyCaseTest() {
        //ARRANGE
        String expected = "1991-22-12 00:00:00";

        //ACT
        String actual = transactionDTO.getDateTime();

        //ASSERT
        assertEquals(expected, actual);

    }

    /**
     * Test for getDescription method
     * Happy Case
     */
    @Test
    void getDescriptionHappyCaseTest() {
        //ARRANGE
        String expected = "diapers";

        //ACT
        String actual = transactionDTO.getDescription();

        //ASSERT
        assertEquals(expected, actual);

    }

    /**
     * Test for getCategory method
     * Happy Case
     */
    @Test
    void getCategoryHappyCaseTest() {
        //ARRANGE
        String expected = "baby stuff";

        //ACT
        String actual = transactionDTO.getCategory();

        //ASSERT
        assertEquals(expected, actual);

    }

    /**
     * Test for getDebitAccountId method
     * Happy Case
     */
    @Test
    void getDebitAccountHappyCaseTest() {
        //ARRANGE
        String expected = "1";

        //ACT
        String actual = transactionDTO.getDebitAccountID();

        //ASSERT
        assertEquals(expected, actual);

    }

    /**
     * Test for getCreditAccountId method
     * Happy Case
     */
    @Test
    void getCreditAccountHappyCaseTest() {
        //ARRANGE
        String expected = "2";

        //ACT
        String actual = transactionDTO.getCreditAccountID();

        //ASSERT
        assertEquals(expected, actual);

    }

    @Test
    void testToString() {
        String expected = "TransactionDTO{amount='1000', dateTime='1991-22-12 00:00:00', type='Debit', description='diapers', category='baby stuff', debitAccountID='1', creditAccountID='2'}";
        //ACTUAL
        String actual = transactionDTO.toString();

        //ASSERT
        assertEquals(expected, actual);
    }

}