package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DuplicateGroupInfoDTOTest {

    private String groupID1;
    private String groupID2;
    private String ledgerID;

    @BeforeEach
    void init() {
        groupID1 = "1";
        groupID2 = "2";
        ledgerID = "11";
    }

    @Test
    void duplicateGroupInfoDTOConstructorTest(){
        DuplicateGroupInfoDTO infoDTO = new DuplicateGroupInfoDTO(groupID1, groupID2, ledgerID);
        assertTrue(infoDTO instanceof DuplicateGroupInfoDTO);
    }

    @Test
    void getGroupID1Test(){
        //ARRANGE
        DuplicateGroupInfoDTO infoDTO = new DuplicateGroupInfoDTO(groupID1, groupID2, ledgerID);
        String expected = "1";
        //ACT
        String result = infoDTO.getGroupID1();
        //ASSERT
        assertEquals(expected, result);
    }

    @Test
    void getGroupID2Test(){
        //ARRANGE
        DuplicateGroupInfoDTO infoDTO = new DuplicateGroupInfoDTO(groupID1, groupID2, ledgerID);
        String expected = "2";
        //ACT
        String result = infoDTO.getGroupID2();
        //ASSERT
        assertEquals(expected, result);
    }

    @Test
    void getLedgerIDTest(){
        //ARRANGE
        DuplicateGroupInfoDTO infoDTO = new DuplicateGroupInfoDTO(groupID1, groupID2, ledgerID);
        String expected = "11";
        //ACT
        String result = infoDTO.getLedgerID();
        //ASSERT
        assertEquals(expected, result);
    }

}