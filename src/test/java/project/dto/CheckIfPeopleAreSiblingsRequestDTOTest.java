package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.entities.shared.PersonID;

import static org.junit.jupiter.api.Assertions.*;

class CheckIfPeopleAreSiblingsRequestDTOTest {

    PersonID personID1;
    PersonID personID2;
    String id1;
    String id2;

    @BeforeEach
    void init() {
        personID1 = new PersonID("12345@switch.pt");
        personID2 = new PersonID("54321@switch.pt");
        id1 = ("12345@switch.pt");
        id2 = ("54321@switch.pt");
    }

    /**
     * Test for PeopleAreSiblingsRequestDTO constructor
     */
    @Test
    @DisplayName("PeopleAreSiblingsRequestDTO - Constructor test")
    void constructorTest() {
        CheckIfPeopleAreSiblingsRequestDTO requestDTO = new CheckIfPeopleAreSiblingsRequestDTO(id1, id2);
        assertTrue(requestDTO instanceof CheckIfPeopleAreSiblingsRequestDTO);
    }

    /**
     * Test for getPersonID1 method
     * Happy Case
     */
    @Test
    @DisplayName("getPersonID1 - Happy Case")
    void getPersonID1HappyCaseTest() {
        //Arrange
        CheckIfPeopleAreSiblingsRequestDTO requestDTO = new CheckIfPeopleAreSiblingsRequestDTO(id1, id2);
        String expected = "12345@switch.pt";

        //Act
        String result = requestDTO.getPersonEmail1();

        //Assert
        assertEquals(expected, result);
    }

    /**
     * Test for getPersonID2 method
     * Happy Case
     */
    @Test
    @DisplayName("getPersonID2 - Happy Case")
    void getPersonID2HappyCaseTest() {
        //Arrange
        CheckIfPeopleAreSiblingsRequestDTO requestDTO = new CheckIfPeopleAreSiblingsRequestDTO(id1, id2);
        String expected = "54321@switch.pt";

        //Act
        String result = requestDTO.getPersonEmail2();

        //Assert
        assertEquals(expected, result);
    }

    /**
     * Test for getPersonID1 method
     * Ensure not equals
     */
    @Test
    @DisplayName("getPersonID1 - Not Equals Case")
    void getPersonID1NotEqualsTest() {
        //Arrange
        CheckIfPeopleAreSiblingsRequestDTO requestDTO = new CheckIfPeopleAreSiblingsRequestDTO(id1, id2);
        String expected = "000000L";

        //Act
        String result = requestDTO.getPersonEmail1();

        //Assert
        assertNotEquals(expected, result);
    }

    /**
     * Test for getPersonID1 method
     * Ensure not equals
     */
    @Test
    @DisplayName("getPersonID2 - Not Equals Case")
    void getPersonID2NotEqualsTest() {
        //Arrange
        CheckIfPeopleAreSiblingsRequestDTO requestDTO = new CheckIfPeopleAreSiblingsRequestDTO(id1, id2);
        String expected = "000000000L";

        //Act
        String result = requestDTO.getPersonEmail2();

        //Assert
        assertNotEquals(expected, result);
    }

    /**
     * Test for equals
     * Ensure true when object is the same
     */
    @Test
    @DisplayName("Equals - Ensure True With Same Object")
    void testEqualsEnsureTrueWithSameObjectTest() {
        //Arrange
        CheckIfPeopleAreSiblingsRequestDTO requestDTO = new CheckIfPeopleAreSiblingsRequestDTO(id1, id2);

        //Act
        boolean result = requestDTO.equals(requestDTO);

        //Assert
        assertTrue(result);
    }

    /**
     * Test for equals
     * Ensure false when comparing different class objects
     */
    @Test
    @DisplayName("Equals - Ensure False With Objects from different classes")
    void testEqualsEnsureFalseDifferentClassTest() {
        //Arrange
        CheckIfPeopleAreSiblingsRequestDTO requestDTO = new CheckIfPeopleAreSiblingsRequestDTO(id1, id2);
        String test = "2";

        //Act
        boolean result = requestDTO.equals(test);

        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure false when comparing with a null object
     */
    @Test
    @DisplayName("Equals - Ensure false with null Test")
    void testEqualsEnsureFalseWithNullTest() {
        //Arrange
        CheckIfPeopleAreSiblingsRequestDTO requestDTO = new CheckIfPeopleAreSiblingsRequestDTO(id1, id2);
        String test = null;

        //Act
        boolean result = requestDTO.equals(test);

        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Happy Case - 2 different objects with same attributes
     */
    @Test
    @DisplayName("Equals - Ensure true with different objects with same attributes")
    void testEqualsHappyCaseTest() {
        //Arrange
        CheckIfPeopleAreSiblingsRequestDTO requestDTO = new CheckIfPeopleAreSiblingsRequestDTO(id1, id2);
        CheckIfPeopleAreSiblingsRequestDTO requestDTO2 = new CheckIfPeopleAreSiblingsRequestDTO(id1, id2);

        //Act
        boolean result = requestDTO.equals(requestDTO2);

        //Assert
        assertTrue(result);
    }

    /**
     * Test for equals
     * Ensure false when Id1 is different
     */
    @Test
    @DisplayName("Equals - Ensure false when Id1 is different")
    void testEqualsEnsureFalseDifferentID1Test() {
        //Arrange
        CheckIfPeopleAreSiblingsRequestDTO requestDTO = new CheckIfPeopleAreSiblingsRequestDTO(id1, id2);
        String differentID1 = "5L";
        CheckIfPeopleAreSiblingsRequestDTO requestDTO2 = new CheckIfPeopleAreSiblingsRequestDTO(differentID1, id2);

        //Act
        boolean result = requestDTO.equals(requestDTO2);

        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure false when Id2 is different
     */
    @Test
    @DisplayName("Equals - Ensure false when Id2 is different")
    void testEqualsEnsureFalseDifferentID2Test() {
        //Arrange
        CheckIfPeopleAreSiblingsRequestDTO requestDTO = new CheckIfPeopleAreSiblingsRequestDTO(id1, id2);
        String differentID2 = "67L";
        CheckIfPeopleAreSiblingsRequestDTO requestDTO2 = new CheckIfPeopleAreSiblingsRequestDTO(id1, differentID2);

        //Act
        boolean result = requestDTO.equals(requestDTO2);

        //Assert
        assertFalse(result);
    }

    /**
     * Test for hashcode
     */
    @Test
    @DisplayName("Test for HashCode")
    void testHashCode() {
        //Arrange
        CheckIfPeopleAreSiblingsRequestDTO requestDTO = new CheckIfPeopleAreSiblingsRequestDTO(id1, id2);
        int expected = 1553502305;

        //Act
        int result = requestDTO.hashCode();

        //Assert
        assertEquals(expected, result);
    }
}