package project.dto;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.entities.group.GroupID;

import static org.junit.jupiter.api.Assertions.*;

class CreateAccountForPersonRequestDTOTest {

    /**
     * Test for CreateAccountForPersonRequestDTO constructor
     */
    @Test
    @DisplayName("Test for CreateAccountForPersonDTO constructor")
    void constructorHappyCaseTest() {
        CreateAccountForPersonRequestDTO dto = new CreateAccountForPersonRequestDTO("1","2","food","food");
        //ASSERT
        assertTrue(dto instanceof CreateAccountForPersonRequestDTO);
    }

    /**
     * Test for getAccountID
     * Happy case
     */
    @Test
    @DisplayName(" Test for getAccountsIDs - Happy Case")
    void getAccountIDHappyCaseTest() {

        //ARRANGE
        CreateAccountForPersonRequestDTO dto = new CreateAccountForPersonRequestDTO("1","2","food","food");
        String expected = "1";
        //ACT
        String result = dto.getAccountID();
        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for getAccountID
     * Happy case
     */
    @Test
    @DisplayName(" Test for getPersonID - Happy Case")
    void getPersonIDHappyCaseTest() {

        //ARRANGE
        CreateAccountForPersonRequestDTO dto = new CreateAccountForPersonRequestDTO("1","2","food","food");
        String expected = "2";
        //ACT
        String result = dto.getPersonEmail();
        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for getDenomination
     * Happy case
     */
    @Test
    @DisplayName(" Test for getDenomination- Happy Case")
    void getDenominationHappyCaseTest() {

        //ARRANGE
        CreateAccountForPersonRequestDTO dto = new CreateAccountForPersonRequestDTO("1","2","food","food");
        String expected = "food";
        //ACT
        String result = dto.getAccountDenomination();
        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for getDescription
     * Happy case
     */
    @Test
    @DisplayName(" Test for getDescription Happy Case")
    void getDescriptionHappyCaseTest() {

        //ARRANGE
        CreateAccountForPersonRequestDTO dto = new CreateAccountForPersonRequestDTO("1","2","food","food");
        String expected = "food";
        //ACT
        String result = dto.getAccountDescription();
        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for equals
     * Happy case
     */
    @Test
    @DisplayName("Test for equals - HappyCase")
    void equalsHappyCaseTest() {
        //Arrange
        CreateAccountForPersonRequestDTO dto1 = new CreateAccountForPersonRequestDTO("1","2","food","food");
        CreateAccountForPersonRequestDTO dto2 = new CreateAccountForPersonRequestDTO("1","2","food","food");
        //Act
        boolean result = dto1.equals(dto2);
        //Assert
        assertTrue(result);
    }

    /**
     * Test for equals
     * Not Equals - Different AccountID
     */
    @Test
    @DisplayName("Test for equals - Different AccountID")
    void equalsDifferentAccountIDTest() {
        //Arrange
        CreateAccountForPersonRequestDTO dto1 = new CreateAccountForPersonRequestDTO("1","2","food","food");
        CreateAccountForPersonRequestDTO dto2 = new CreateAccountForPersonRequestDTO("2","2","food","food");
        //Act
        boolean result = dto1.equals(dto2);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Not Equals - Different PersonID
     */
    @Test
    @DisplayName("Test for equals - Different PersonID")
    void equalsDifferentPersonIDTest() {
        //Arrange
        CreateAccountForPersonRequestDTO dto1 = new CreateAccountForPersonRequestDTO("1","2","food","food");
        CreateAccountForPersonRequestDTO dto2 = new CreateAccountForPersonRequestDTO("1","1","food","food");
        //Act
        boolean result = dto1.equals(dto2);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Not Equals - Different Denomination
     */
    @Test
    @DisplayName("Test for equals - Different Denomination")
    void equalsDifferentDenominationTest() {
        //Arrange
        CreateAccountForPersonRequestDTO dto1 = new CreateAccountForPersonRequestDTO("1","2","food","food");
        CreateAccountForPersonRequestDTO dto2 = new CreateAccountForPersonRequestDTO("1","2","comida","food");
        //Act
        boolean result = dto1.equals(dto2);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Not Equals - Different Description
     */
    @Test
    @DisplayName("Test for equals - Different Description")
    void equalsDifferentDescriptionTest() {
        //Arrange
        CreateAccountForPersonRequestDTO dto1 = new CreateAccountForPersonRequestDTO("1","2","food","food");
        CreateAccountForPersonRequestDTO dto2 = new CreateAccountForPersonRequestDTO("1","2","food","comida");
        //Act
        boolean result = dto1.equals(dto2);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure false with null object
     */
    @Test
    @DisplayName("Test for equals - Ensure false with null")
    void equalsEnsureFalseWithNullTest() {
        //Arrange
        CreateAccountForPersonRequestDTO dto = new CreateAccountForPersonRequestDTO("1","2","food","food");
        CreateAccountForPersonRequestDTO dto2 = null;
        //Act
        boolean result = dto.equals(dto2);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure false with diferent object
     */
    @Test
    @DisplayName("Test for equals - Ensure false with object from another class")
    void equalsEnsureFalseWithObjectFromAnotherClassTest() {
        //Arrange
        CreateAccountForPersonRequestDTO dto = new CreateAccountForPersonRequestDTO("1","2","food","food");
        GroupID groupID = new GroupID("1");
        //Act
        boolean result = dto.equals(groupID);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure true if is same object
     */
    @Test
    @DisplayName("Test for equals - Ensure true with same object")
    void equalsEnsureTrueWithSameObjectTest() {
        //Arrange
        CreateAccountForPersonRequestDTO dto = new CreateAccountForPersonRequestDTO("1","2","food","food");
        //Act
        boolean result = dto.equals(dto);
        //Assert
        assertTrue(result);
    }

    /**
     * Test for hashcode
     * Ensure works
     */
    @Test
    @DisplayName("Test for hashCode - Ensure works")
    void hashCodeEnsureWorksTest() {
        //Arrange
        CreateAccountForPersonRequestDTO dto = new CreateAccountForPersonRequestDTO("1","2","food","food");
        int expected = 103195938;
        //Act
        int result = dto.hashCode();
        //Assert
        assertEquals(expected, result);
    }

}