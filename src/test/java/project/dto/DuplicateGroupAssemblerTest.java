package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import project.model.entities.group.Group;
import project.model.entities.group.GroupID;
import project.model.entities.shared.LedgerID;
import project.model.entities.shared.PersonID;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DuplicateGroupAssemblerTest {

    private PersonID creatorID;
    private GroupID groupID;
    private LedgerID ledgerID;

    @BeforeEach
    void init() {
        creatorID = new PersonID("goncalo@switch.pt");
        groupID = new GroupID("1");
        ledgerID = new LedgerID("1");
    }

    @Test
    void mapToResponseDTOHappyCaseTest() {
        //ARRANGE
        Group group = new Group(groupID, "Family", "2020-07-06", creatorID, ledgerID);
        group.addCategory("Uncles");

        Set<String> expectedMembers = new HashSet<>();
        expectedMembers.add("goncalo@switch.pt");

        Set<String> expectedManagers = new HashSet<>();
        expectedManagers.add("goncalo@switch.pt");

        Set<String> expectedCategories = new HashSet<>();
        expectedCategories.add("Uncles");

        String expectedGroupID = "1";
        String expectedLedgerID = "1";
        String expectedDescription = "Family";
        String expectedCreationDate = "2020-07-06";

        DuplicateGroupResponseDTO expected = new DuplicateGroupResponseDTO(expectedMembers, expectedManagers,
                expectedCategories, expectedGroupID, expectedLedgerID, expectedDescription, expectedCreationDate);
        //ACT
        DuplicateGroupResponseDTO result = DuplicateGroupAssembler.mapToResponseDTO(group);
        //ASSERT
        assertEquals(expected, result);
    }

    @Test
    void mapToRequestDTOHappyCaseTest() {
        //ARRANGE

        String personID = "goncalo@switch.pt";
        String groupID = "1";
        String groupID2 = "2";
        String ledgerID = "1";

        DuplicateGroupRequestDTO expected = new DuplicateGroupRequestDTO(groupID, groupID2, ledgerID, personID);
        //ACT
        DuplicateGroupRequestDTO result = DuplicateGroupAssembler.mapToRequestDTO(groupID, groupID2, ledgerID, personID);
        //ASSERT
        assertEquals(expected, result);
    }


}