package project.dto;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.entities.group.GroupID;

import static org.junit.jupiter.api.Assertions.*;

class AddMemberResponseDTOTest {

    /**
     * Test for AddMemberResponseDTO constructor
     * Happy case
     */
    @DisplayName("Test for constructor - Happy case")
    @Test
    void constructorHappyCaseTest() {

        AddMemberResponseDTO dto = new AddMemberResponseDTO("22", "family", "11");

        //ASSERT
        assertTrue(dto instanceof AddMemberResponseDTO);
    }

    /**
     * Test for getNewMemberID
     * Happy case
     */
    @DisplayName("Test for getNewMemberID - Happy case")
    @Test
    void getNewMemberIDHappyCaseTest() {

        //ARRANGE
        String newMemberIDExpected = "11";
        AddMemberResponseDTO dto = new AddMemberResponseDTO("22", "family", "11");

        //ACT
        String result = dto.getNewMemberEmail();

        //ASSERT
        assertEquals(newMemberIDExpected, result);
    }

    /**
     * Test for getGroupID
     * Happy case
     */
    @DisplayName("Test for getGroupID - Happy case")
    @Test
    void getGroupIDHappyCaseTest() {

        //ARRANGE
        String getGroupIDExpected = "22";
        AddMemberResponseDTO dto = new AddMemberResponseDTO("22", "family", "11");

        //ACT
        String result = dto.getGroupID();

        //ASSERT
        assertEquals(getGroupIDExpected, result);
    }

    /**
     * Test for getGroupDescription
     * Happy case
     */
    @DisplayName("Test for getGroupDescription - Happy case")
    @Test
    void getGroupDescriptionHappyCaseTest() {

        //ARRANGE
        String getGroupDescriptionExpected = "family";
        AddMemberResponseDTO dto = new AddMemberResponseDTO("22", "family", "11");

        //ACT
        String result = dto.getGroupDescription();

        //ASSERT
        assertEquals(getGroupDescriptionExpected, result);
    }

    /**
     * Test for equals
     * Happy case
     */
    @DisplayName("Test for equals - Happy case")
    @Test
    void equalsHappyCaseTest() {

        //ARRANGE
        AddMemberResponseDTO dto = new AddMemberResponseDTO("22", "family", "11");

        //ACT
        boolean result = dto.equals(dto);

        //ASSERT
        assertTrue(result);
    }

    /**
     * Test for equals
     * Ensure false with null
     */
    @DisplayName("Test for equals - Ensure false with null")
    @Test
    void equalsEnsureFalseWithNullCaseTest() {

        //ARRANGE
        AddMemberResponseDTO dto = new AddMemberResponseDTO("22", "family", "11");

        //ACT
        boolean result = dto.equals(null);

        //ASSERT
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure false with different groupID
     */
    @DisplayName("Test for equals - Ensure false with different groupID")
    @Test
    void equalsEnsureFalseWithDifferentGroupIDCaseTest() {

        //ARRANGE
        AddMemberResponseDTO dto = new AddMemberResponseDTO("22", "family", "11");
        AddMemberResponseDTO dto2 = new AddMemberResponseDTO("23", "family", "11");

        //ACT
        boolean result = dto.equals(dto2);

        //ASSERT
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure false with different groupDescription
     */
    @DisplayName("Test for equals - Ensure false with different groupDescription")
    @Test
    void equalsEnsureFalseWithDifferentGroupDescriptionCaseTest() {

        //ARRANGE
        AddMemberResponseDTO dto = new AddMemberResponseDTO("22", "family", "11");
        AddMemberResponseDTO dto2 = new AddMemberResponseDTO("22", "Dinner", "11");

        //ACT
        boolean result = dto.equals(dto2);

        //ASSERT
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure false with different memberID
     */
    @DisplayName("Test for equals - Ensure false with different memberID")
    @Test
    void equalsEnsureFalseWithDifferentMemberIDCaseTest() {

        //ARRANGE
        AddMemberResponseDTO dto = new AddMemberResponseDTO("22", "family", "11");
        AddMemberResponseDTO dto2 = new AddMemberResponseDTO("22", "family", "12");

        //ACT
        boolean result = dto.equals(dto2);

        //ASSERT
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure false with different Object
     */
    @DisplayName("Test for equals - Ensure false with different Object")
    @Test
    void equalsEnsureFalseWithDifferentObjectCaseTest() {

        //ARRANGE
        AddMemberResponseDTO dto = new AddMemberResponseDTO("22", "family", "11");
        GroupID groupID = new GroupID("11");

        //ACT
        boolean result = dto.equals(groupID);

        //ASSERT
        assertFalse(result);
    }

    /**
     * Test for hashCode
     * Happy Case
     */
    @DisplayName("Test for hashCode - Happy Case")
    @Test
    void hashCodeHappyCaseTest() {

        //ARRANGE
        AddMemberResponseDTO dto = new AddMemberResponseDTO("22", "family", "11");
        int expected = -1279351004;

        //ACT
        int result = dto.hashCode();

        //ASSERT
        assertEquals(expected, result);
    }
}