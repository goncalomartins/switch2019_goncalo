package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DuplicateGroupRequestDTOTest {

    private String groupID1;
    private String groupID2;
    private String ledgerID;
    private String personEmail;

    @BeforeEach
    void init() {
        groupID1 = "1";
        groupID2 = "2";
        ledgerID = "11";
        personEmail = "goncalo@switch.pt";
    }

    @Test
    void duplicateGroupRequestDTOConstructorTest(){
        DuplicateGroupRequestDTO requestDTO = new DuplicateGroupRequestDTO(groupID1, groupID2, ledgerID, personEmail);
        assertTrue(requestDTO instanceof DuplicateGroupRequestDTO);
    }

    @Test
    void getGroupID1Test(){
        //ARRANGE
        DuplicateGroupRequestDTO requestDTO = new DuplicateGroupRequestDTO(groupID1, groupID2, ledgerID, personEmail);
        String expected = "1";
        //ACT
        String result = requestDTO.getGroupID1();
        //ASSERT
        assertEquals(expected, result);
    }

    @Test
    void getGroupID2Test(){
        //ARRANGE
        DuplicateGroupRequestDTO requestDTO = new DuplicateGroupRequestDTO(groupID1, groupID2, ledgerID, personEmail);
        String expected = "2";
        //ACT
        String result = requestDTO.getGroupID2();
        //ASSERT
        assertEquals(expected, result);
    }

    @Test
    void getLedgerIDTest(){
        //ARRANGE
        DuplicateGroupRequestDTO requestDTO = new DuplicateGroupRequestDTO(groupID1, groupID2, ledgerID, personEmail);
        String expected = "11";
        //ACT
        String result = requestDTO.getLedgerID();
        //ASSERT
        assertEquals(expected, result);
    }

    @Test
    void getPersonEmailTest(){
        //ARRANGE
        DuplicateGroupRequestDTO requestDTO = new DuplicateGroupRequestDTO(groupID1, groupID2, ledgerID, personEmail);
        String expected = "goncalo@switch.pt";
        //ACT
        String result = requestDTO.getPersonEmail();
        //ASSERT
        assertEquals(expected, result);
    }

}