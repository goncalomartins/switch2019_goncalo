package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.entities.group.Group;
import project.model.entities.group.GroupID;
import project.model.entities.person.Person;
import project.model.entities.shared.LedgerID;
import project.model.entities.shared.PersonID;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class AddMemberAssemblerTest {

    private PersonID creatorID;
    private PersonID memberID;
    private GroupID newGroupID;
    private LedgerID newLedgerID;
    private Group newGroup;

    @BeforeEach
    public void initialize() {
        creatorID = new PersonID("11@switch.pt");
        memberID = new PersonID("12@switch.pt");
        newGroupID = new GroupID("22");
        newLedgerID = new LedgerID("33");

        newGroup = new Group(newGroupID, "isep", "2020-05-21", creatorID, newLedgerID);
        newGroup.addMemberID(memberID);

    }

    /**
     * Test for AddMemberAssembler Constructor
     */
    @Test
    @DisplayName("Test for AddMemberAssembler Constructor")
    void addMemberAssemblerConstructor() {
        AddMemberAssembler assembler = new AddMemberAssembler();
        assertTrue(assembler instanceof AddMemberAssembler);
    }

    /**
     * Test for mapToRequestDTO Method
     * Happy case
     */
    @Test
    @DisplayName("Test for mapToRequestDTO - Happy Case")
    void mapToRequestDTOHappyCaseTest() {
        //ARRANGE
        String newMemberID = "11@switch.pt";
        String groupID = "22";
        AddMemberRequestDTO expected = new AddMemberRequestDTO(newMemberID, groupID);

        //ACT
        AddMemberRequestDTO result = AddMemberAssembler.mapToRequestDTO(newMemberID, groupID);

        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for mapToRequestDTO Method
     * Happy case
     */
    @Test
    @DisplayName("Test for mapToRequestDTO - Sad Case ")
    void mapToRequestDTOSadCaseTest() {
        //ARRANGE
        String newMemberID = "11@switch.pt";
        String groupID = "22";
        String expectedGroupID = "23";
        AddMemberRequestDTO expected = new AddMemberRequestDTO(newMemberID, expectedGroupID);

        //ACT
        AddMemberRequestDTO result = AddMemberAssembler.mapToRequestDTO(newMemberID, groupID);

        //ASSERT
        assertNotEquals(expected, result);
    }

    /**
     * Test for mapToDTO Method
     * with parameter Group
     * Happy case
     */
    @Test
    @DisplayName("Test for mapToDTO with parameter Group - Happy Case")
    void mapToResponseDTOWithGroupHappyCaseTest() {
        //ARRANGE
        List<String> expectedIdInStrings = new ArrayList<>();
        expectedIdInStrings.add("11@switch.pt");
        expectedIdInStrings.add("12@switch.pt");
        List<String> namesInString = new ArrayList<>();
        namesInString.add("carlos");
        namesInString.add("maria");
        List<Boolean> isManagerInString = new ArrayList<>();
        isManagerInString.add(true);
        isManagerInString.add(false);


        MembersDTO expected = new MembersDTO(expectedIdInStrings, namesInString, isManagerInString);

        Set<Person> personResult = new HashSet<>();
        personResult.add(new Person(new PersonID("11@switch.pt"), "carlos", "rua da lua", "porto", "1996-04-25", null, null, new LedgerID("12")));
        personResult.add(new Person(new PersonID("12@switch.pt"), "maria", "rua da lua", "porto", "1996-04-25", null, null, new LedgerID("13")));

        //ACT
        MembersDTO result = AddMemberAssembler.mapToResponseDTO(newGroup, personResult);

        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for mapToDTO Method
     * with parameter Group
     * Sad case
     */
    @Test
    @DisplayName("Test for mapToDTO with parameter Group - Sad Case")
    void mapToResponseDTOWithGroupSadCaseTest() {
        //ARRANGE
        List<String> expectedIdInStrings = new ArrayList<>();
        expectedIdInStrings.add("11@switch.pt");

        List<String> namesInString = new ArrayList<>();
        namesInString.add("carlos");

        List<Boolean> isManagerInString = new ArrayList<>();
        isManagerInString.add(true);


        MembersDTO expected = new MembersDTO(expectedIdInStrings, namesInString, isManagerInString);

        Set<Person> personResult = new HashSet<>();
        personResult.add(new Person(new PersonID("11@switch.pt"), "carlos", "rua da lua", "porto", "1996-04-25", null, null, new LedgerID("12")));
        personResult.add(new Person(new PersonID("12@switch.pt"), "maria", "rua da lua", "porto", "1996-04-25", null, null, new LedgerID("13")));

        //ACT
        MembersDTO result = AddMemberAssembler.mapToResponseDTO(newGroup, personResult);

        //ASSERT
        assertNotEquals(expected, result);
    }

    /**
     * Test for mapToDTO Method
     * with parameter String groupID, String groupDescription, String personID
     * Happy Case
     */
    @Test
    @DisplayName("Test for mapToDTO with parameter String groupID, String groupDescription, String personID - Happy Case")
    void mapToResponseDTOHappyCaseTest() {
        //ARRANGE
        AddMemberResponseDTO expected = new AddMemberResponseDTO("22", "isep", "11@switch.pt");

        //ACT
        AddMemberResponseDTO result = AddMemberAssembler.mapToResponseDTO("22", "isep", "11@switch.pt");

        //ASSERT
        assertEquals(expected, result);
    }
}