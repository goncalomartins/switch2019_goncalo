package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CreateGroupResponseDTOTest {

    private String groupID;
    private String description;

    @BeforeEach
    void init() {
        groupID = "1L";
        description = "description";
    }

    /**
     * Test for CreateGroupResponseDTO constructor
     */
    @Test
    @DisplayName("Test for CreateGroupResponseDTO constructor - Happy Case")
    void constructorTest() {
        CreateGroupResponseDTO groupDTO = new CreateGroupResponseDTO(groupID, description);
        assertTrue(groupDTO instanceof CreateGroupResponseDTO);
    }

    /**
     * Test for getGroupID method
     * <p>
     * Happy Case
     */
    @Test
    void getGroupIDHappyCaseTest() {
        //Arrange
        CreateGroupResponseDTO groupDTO = new CreateGroupResponseDTO(groupID, description);

        String expected = "1L";

        //Act
        String result = groupDTO.getGroupID();

        //Assert
        assertEquals(expected, result);

    }

    /**
     * Test for getGroupID method
     * <p>
     * Ensure not equals
     */
    @Test
    void getGroupIDEnsureNotEquals() {
        //Arrange
        CreateGroupResponseDTO groupDTO = new CreateGroupResponseDTO(groupID, description);

        String expected = "2L";

        //Act
        String result = groupDTO.getGroupID();

        //Assert
        assertNotEquals(expected, result);

    }

    /**
     * Test for getDescription method
     * <p>
     * Happy Case
     */
    @Test
    void getDescriptionHappyCaseTest() {
        //Arrange
        CreateGroupResponseDTO groupDTO = new CreateGroupResponseDTO(groupID, description);

        String expected = "description";

        //Act
        String result = groupDTO.getDescription();

        //Assert
        assertEquals(expected, result);

    }

    /**
     * Test for getDescription method
     * <p>
     * Ensure not equals
     */
    @Test
    void getDescriptionEnsureNotEqualsTest() {
        //Arrange
        CreateGroupResponseDTO groupDTO = new CreateGroupResponseDTO(groupID, description);

        String expected = "different description";

        //Act
        String result = groupDTO.getDescription();

        //Assert
        assertNotEquals(expected, result);

    }

    /**
     * Test for equals
     * <p>
     * Happy Case - Comparing same object
     */
    @Test
    void testEqualsHappyCaseTest() {
        //Arrange
        CreateGroupResponseDTO groupDTO = new CreateGroupResponseDTO(groupID, description);

        //Act
        boolean result = groupDTO.equals(groupDTO);

        //Assert
        assertTrue(result);
    }

    /**
     * Test for equals
     * <p>
     * Happy Case - 2 different CreateGroupResponseDTO  objects with same attributes
     */
    @Test
    void testEqualsHappyCaseDifferentObjectsSameAttributesTest() {
        //Arrange
        CreateGroupResponseDTO groupDTO = new CreateGroupResponseDTO(groupID, description);

        CreateGroupResponseDTO otherCreateGroupResponseDTO = new CreateGroupResponseDTO(groupID, description);

        //Act
        boolean result = groupDTO.equals(otherCreateGroupResponseDTO);

        //Assert
        assertTrue(result);
    }

    /**
     * Test for equals
     * <p>
     * Ensure false if one of the objects is null
     */
    @Test
    void testEqualsEnsureFalseIfOtherIsNullTest() {
        //Arrange
        CreateGroupResponseDTO groupDTO = new CreateGroupResponseDTO(groupID, description);

        CreateGroupResponseDTO otherCreateGroupResponseDTO = null;

        //Act
        boolean result = groupDTO.equals(otherCreateGroupResponseDTO);

        //Assert
        assertFalse(result);

    }

    /**
     * Test for equals
     * <p>
     * Ensure false if class is different
     */
    @Test
    void testEqualsEnsureFalseIfClassIsDifferentTest() {
        //Arrange
        CreateGroupResponseDTO groupDTO = new CreateGroupResponseDTO(groupID, description);

        String other = "other";

        //Act
        boolean result = groupDTO.equals(other);

        //Assert
        assertFalse(result);

    }

    /**
     * Test for equals
     * <p>
     * Ensure false if groupID is different
     */
    @Test
    void testEqualsEnsureFalseDifferentGroupIDTest() {
        //Arrange
        CreateGroupResponseDTO groupDTO = new CreateGroupResponseDTO(groupID, description);

        String otherGroupID = "10L";
        CreateGroupResponseDTO otherCreateGroupResponseDTO = new CreateGroupResponseDTO(otherGroupID, description);

        //Act
        boolean result = groupDTO.equals(otherCreateGroupResponseDTO);

        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * <p>
     * Ensure false if description is different
     */
    @Test
    void testEqualsEnsureFalseDifferentDescriptionTest() {
        //Arrange
        CreateGroupResponseDTO groupDTO = new CreateGroupResponseDTO(groupID, description);

        String otherDescription = "other description";
        CreateGroupResponseDTO otherCreateGroupResponseDTO = new CreateGroupResponseDTO(groupID, otherDescription);

        //Act
        boolean result = groupDTO.equals(otherCreateGroupResponseDTO);

        //Assert
        assertFalse(result);
    }

    @Test
    void testHashCode() {
        //Arrange
        CreateGroupResponseDTO groupDTO = new CreateGroupResponseDTO(groupID, description);
        int expected = -1724465855;

        //Act
        int result = groupDTO.hashCode();

        //Assert
        assertEquals(expected, result);

    }

}