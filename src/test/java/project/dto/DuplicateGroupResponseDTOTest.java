package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class DuplicateGroupResponseDTOTest {

    private Set<String> members;
    private Set<String> managers;
    private Set<String> categories;
    private String creatorID;
    private String memberID;
    private String groupID;
    private String ledgerID;
    private String description;
    private String creationDate;

    @BeforeEach
    void init() {
        members = new HashSet<>();
        creatorID = "goncalo@switch.pt";
        memberID = "joao@switch.pt";
        members.add(creatorID);
        members.add(memberID);
        managers = new HashSet<>();
        managers.add(creatorID);
        categories = new HashSet<>();
        categories.add("Uncles");
        groupID = "1";
        ledgerID = "11";
        description = "Family";
        creationDate = "2020-07-06";
    }

    @Test
    void constructorTest() {
        DuplicateGroupResponseDTO responseDTO = new DuplicateGroupResponseDTO(members, managers, categories, groupID, ledgerID, description, creationDate);
        assertTrue(responseDTO instanceof DuplicateGroupResponseDTO);
    }

    @Test
    void getMembersHappyCaseTest() {
        //Arrange
        DuplicateGroupResponseDTO responseDTO = new DuplicateGroupResponseDTO(members, managers, categories, groupID, ledgerID, description, creationDate);

        Set<String> expected = new HashSet<>();
        expected.add("goncalo@switch.pt");
        expected.add("joao@switch.pt");

        //Act
        Set<String> result = responseDTO.getMembers();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void getManagersHappyCaseTest() {
        //Arrange
        DuplicateGroupResponseDTO responseDTO = new DuplicateGroupResponseDTO(members, managers, categories, groupID, ledgerID, description, creationDate);

        Set<String> expected = new HashSet<>();
        expected.add("goncalo@switch.pt");

        //Act
        Set<String> result = responseDTO.getManagers();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void getCategoriesHappyCaseTest() {
        //Arrange
        DuplicateGroupResponseDTO responseDTO = new DuplicateGroupResponseDTO(members, managers, categories, groupID, ledgerID, description, creationDate);

        Set<String> expected = new HashSet<>();
        expected.add("Uncles");

        //Act
        Set<String> result = responseDTO.getCategories();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void getGroupIDHappyCaseTest() {
        //Arrange
        DuplicateGroupResponseDTO responseDTO = new DuplicateGroupResponseDTO(members, managers, categories, groupID, ledgerID, description, creationDate);

        String expected = "1";

        //Act
        String result = responseDTO.getGroupID();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void getLedgerIDHappyCaseTest() {
        //Arrange
        DuplicateGroupResponseDTO responseDTO = new DuplicateGroupResponseDTO(members, managers, categories, groupID, ledgerID, description, creationDate);

        String expected = "11";

        //Act
        String result = responseDTO.getLedgerID();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void getDescriptionHappyCaseTest() {
        //Arrange
        DuplicateGroupResponseDTO responseDTO = new DuplicateGroupResponseDTO(members, managers, categories, groupID, ledgerID, description, creationDate);

        String expected = "Family";

        //Act
        String result = responseDTO.getDescription();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void getCreationDateHappyCaseTest() {
        //Arrange
        DuplicateGroupResponseDTO responseDTO = new DuplicateGroupResponseDTO(members, managers, categories, groupID, ledgerID, description, creationDate);

        String expected = "2020-07-06";

        //Act
        String result = responseDTO.getCreationDate();

        //Assert
        assertEquals(expected, result);
    }
}