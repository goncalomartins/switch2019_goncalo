package project.infrastructure.repositories;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import project.model.entities.group.Group;
import project.model.entities.group.GroupID;
import project.model.entities.shared.LedgerID;
import project.model.entities.shared.PersonID;
import project.model.specifications.repositories.GroupRepository;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Transactional
class GroupRepositoryDBTest {

    @Autowired
    GroupRepository groupRepository;

    /**
     * Test for constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for groupRepositoryDB constructor - HappyCase")
    void constructorHappyCaseTest() {
        assertTrue(groupRepository instanceof GroupRepositoryDB);
    }

    /**
     * Test for save
     * Happy case
     */
    @Test
    @DisplayName("Test for GroupRepository save - HappyCase")
    void saveHappyCaseGroupRepositoryTest() {
        // Arrange
        PersonID creatorID1 = new PersonID("1@switch.pt");
        LedgerID ledgerIDg1 = new LedgerID("1");

        GroupID groupID1 = new GroupID("1");
        Group expected = new Group(groupID1, "Description1", "2020-01-01", creatorID1, ledgerIDg1);

        // Act
        Group actual = groupRepository.save(expected);

        // Assert
        assertEquals(expected, actual);
    }

    /**
     * Test for findById method
     * HappyCase
     */
    @Test
    @DisplayName("Test for findById - HappyCase")
    void findByIdHappyCaseTest() {
        //Arrange
        PersonID creatorID1 = new PersonID("1@switch.pt");
        LedgerID ledgerIDg1 = new LedgerID("1");

        GroupID groupID1 = new GroupID("1");
        Group group1 = new Group(groupID1, "Description1", "2020-01-01", creatorID1, ledgerIDg1);

        groupRepository.save(group1);

        Optional<Group> expected = Optional.of(group1);

        //Act
        Optional<Group> actual = groupRepository.findById(groupID1);

        //Assert
        assertEquals(expected, actual);
    }

    /**
     * Test for findById method
     * Ensure throws exception when the group to be found is not in the repository
     */
    @Test
    @DisplayName("Test for findById - Ensure exception when the group to be found is not in the repository")
    void findByIDEnsureExceptionWhenGroupNotFoundTest() {
        //Arrange
        GroupID groupID3 = new GroupID("3");
        //Act
        Optional<Group> actual = groupRepository.findById(groupID3);
        //Assert
        assertFalse(actual.isPresent());
    }

    /**
     * Test for findById
     * Ensure works with null test
     */
    @Test
    @DisplayName("Test for findById - GroupRepository - Ensure works with null test")
    void findByIdEnsureWorksWithNullGroupRepositoryTest() {
        // Arrange
        Optional<Group> expected = Optional.empty();

        // Act
        Optional<Group> actual = groupRepository.findById(null);

        // Assert
        assertEquals(expected, actual);
    }

    /**
     * Test for findAll method
     * <p>
     * We test for number of groups in repository
     * If new groups are added or removed in projectApplication this test must be revised
     */
    @Test
    @DisplayName("Test for findAll - Happy Case")
    void findAllHappyCase() {
        //Arrange
        int expected = 19;

        // Act
        int actual = groupRepository.findAll().size();

        //
        assertEquals(expected, actual);
    }
}