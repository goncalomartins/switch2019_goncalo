package project.datamodel;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import project.model.entities.ledger.Amount;
import project.model.entities.shared.AccountID;
import project.model.entities.shared.Category;
import project.model.entities.shared.Description;
import project.model.entities.shared.LedgerID;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class TransactionJpaTest {

    private LedgerJpa ledgerJpa;
    private Amount amount;
    private Description description;
    private Category category;
    private AccountID debitAccountId;
    private AccountID creditAccountId;
    private TransactionJpa transactionJpa;
    private String dateTime;
    private String type;

    @BeforeEach
    public void init() {
        dateTime = "2020-01-01 00:00:00";
        type = "Debit";
        ledgerJpa = new LedgerJpa("100");
        amount = new Amount("1000");
        description = new Description("description");
        category = new Category("designation");
        debitAccountId = new AccountID("10");
        creditAccountId = new AccountID("20");
        transactionJpa = new TransactionJpa(ledgerJpa, amount, dateTime, type, description, category, debitAccountId, creditAccountId);
    }

    @Test
    void constructorHappyCaseTest() {
        //Arrange
        //Act
        //Assert
        assertTrue(transactionJpa instanceof TransactionJpa);
    }

    @Test
    void constructorEmptyHappyCaseTest() {
        //Arrange
        TransactionJpa transactionJpa1 = new TransactionJpa();
        //Act
        //Assert
        assertTrue(transactionJpa1 instanceof TransactionJpa);
    }

    @Test
    void toStringHappyCaseTest() {
        //Arrange
        String expected = "TransactionJpa{id=0, amount=Amount{amount=1000.0}, dateTime='2020-01-01 00:00:00', type='Debit', description=Description{description='description'}, category=Category{designation=Designation{designation='designation'}}, debitAccountId=AccountID{accountID=10}, creditAccountId=AccountID{accountID=20}}";

        //Act
        String actual = transactionJpa.toString();

        //Assert
        assertEquals(expected, actual);
    }

    @Test
    void hashCodeTest() {
        //Arrange
        int expected = -680645377;

        //Act
        int actual = transactionJpa.hashCode();

        //Assert
        assertEquals(expected, actual);
    }

    @Test
    void setId() {
        //Arrange
        long expected = 1;
        transactionJpa.setId(expected);

        //Act
        long actual = transactionJpa.getId();

        //Assert
        assertEquals(expected, actual);

    }

    @Test
    void setLedgerJpa() {
        //Arrange
        LedgerJpa expected = new LedgerJpa();
        expected.setId(new LedgerID("100"));
        expected.setTransactions(new ArrayList<>());
        transactionJpa.setLedgerJpa(expected);

        //Act
        LedgerJpa actual = transactionJpa.getLedgerJpa();

        //Assert
        assertEquals(expected, actual);

    }

    @Test
    void setAmount() {
        //Arrange
        Amount expected = new Amount("1000");
        transactionJpa.setAmount(expected);

        //Act
        Amount actual = transactionJpa.getAmount();

        //Assert
        assertEquals(expected, actual);

    }

    @Test
    void setDateTime() {
        //Arrange
        String expected = "2020-01-01 00:00:00";
        transactionJpa.setDateTime(expected);

        //Act
        String actual = transactionJpa.getDateTime();

        //Assert
        assertEquals(expected, actual);

    }

    @Test
    void setType() {
        //Arrange
        String expected = "Debit";
        transactionJpa.setType(expected);

        //Act
        String actual = transactionJpa.getType();

        //Assert
        assertEquals(expected, actual);

    }

    @Test
    void setDescription() {
        //Arrange
        Description expected = new Description("description");
        transactionJpa.setDescription(expected);

        //Act
        Description actual = transactionJpa.getDescription();

        //Assert
        assertEquals(expected, actual);
    }

    @Test
    void setCategory() {
        //Arrange
        Category expected = new Category("designation");
        transactionJpa.setCategory(expected);

        //Act
        Category actual = transactionJpa.getCategory();

        //Assert
        assertEquals(expected, actual);

    }

    @Test
    void setDebitAccountId() {
        //Arrange
        AccountID expected = new AccountID("10");
        transactionJpa.setDebitAccountId(expected);

        //Act
        AccountID actual = transactionJpa.getDebitAccountId();

        //Assert
        assertEquals(expected, actual);
    }

    @Test
    void setCreditAccountId() {
        //Arrange
        AccountID expected = new AccountID("20");
        transactionJpa.setCreditAccountId(expected);

        //Act
        AccountID actual = transactionJpa.getCreditAccountId();

        //Assert
        assertEquals(expected, actual);
    }

    @Test
    void getId() {
        //Arrange
        long expected = 0;

        //Act
        long actual = transactionJpa.getId();

        //Assert
        assertEquals(expected, actual);

    }

    @Test
    void getLedgerJpa() {
        //Arrange
        LedgerJpa expected = new LedgerJpa();
        expected.setId(new LedgerID("100"));
        expected.setTransactions(new ArrayList<>());

        //Act
        LedgerJpa actual = transactionJpa.getLedgerJpa();

        //Assert
        assertEquals(expected, actual);

    }

    @Test
    void getAmount() {
        //Arrange
        Amount expected = new Amount("1000");

        //Act
        Amount actual = transactionJpa.getAmount();

        //Assert
        assertEquals(expected, actual);

    }

    @Test
    void getDateTime() {
        //Arrange
        String expected = "2020-01-01 00:00:00";

        //Act
        String actual = transactionJpa.getDateTime();

        //Assert
        assertEquals(expected, actual);

    }

    @Test
    void getType() {
        //Arrange
        String expected = "Debit";

        //Act
        String actual = transactionJpa.getType();

        //Assert
        assertEquals(expected, actual);

    }

    @Test
    void getDescription() {
        //Arrange
        Description expected = new Description("description");

        //Act
        Description actual = transactionJpa.getDescription();

        //Assert
        assertEquals(expected, actual);
    }

    @Test
    void getCategory() {
        //Arrange
        Category expected = new Category("designation");

        //Act
        Category actual = transactionJpa.getCategory();

        //Assert
        assertEquals(expected, actual);

    }

    @Test
    void getDebitAccountId() {
        //Arrange
        AccountID expected = new AccountID("10");

        //Act
        AccountID actual = transactionJpa.getDebitAccountId();

        //Assert
        assertEquals(expected, actual);
    }

    @Test
    void getCreditAccountId() {
        //Arrange
        AccountID expected = new AccountID("20");

        //Act
        AccountID actual = transactionJpa.getCreditAccountId();

        //Assert
        assertEquals(expected, actual);
    }


}