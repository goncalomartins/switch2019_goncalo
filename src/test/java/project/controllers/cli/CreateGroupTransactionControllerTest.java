//package project.controllers;
//
//import org.junit.jupiter.api.DisplayName;
//import org.junit.jupiter.api.Test;
//import project.model.domainentities.account.Account;
//import project.model.domainentities.group.Group;
//import project.model.domainentities.ledger.Type;
//import project.model.domainentities.person.Person;
//import project.model.domainentities.shared.Category;
//
//import static org.junit.jupiter.api.Assertions.assertFalse;
//import static org.junit.jupiter.api.Assertions.assertTrue;
//
//class CreateGroupTransactionControllerTest {
//
//    /**
//     * Test for CreateGroupTransactionController
//     * Trying to add a transaction
//     */
//    @Test
//    @DisplayName("Test for CreateGroupTransactionController - HappyCase")
//    void createGroupTransactionControllerTestHappyCase() {
//        //ARRANGE
//        CreateGroupTransactionController control = new CreateGroupTransactionController();
//        Person joao = new Person("João", "Travessa de santa Bárbara", "Aveiro", "1987-04-17", null, null);
//        Group g1 = new Group("Saude", "2018-01-26", joao);
//
//        Category c1 = new Category("health");
//
//        Account debit = new Account("pills", "pay pills");
//        Account credit = new Account("pharmacy", "pharmacy from main street");
//
//        g1.addCategory("health");
//        g1.addAccount("pills", "pay pills");
//        g1.addAccount("pharmacy", "pharmacy from main street");
//
//        //ACT
//        boolean result = control.addTransaction(g1, 20.50, Type.DEBIT, "2020-12-01 00:00:00", "health", c1, debit, credit);
//
//        //ASSERT
//        assertTrue(result);
//
//    }
//
//    /**
//     * Test for CreateGroupTransactionController
//     * Trying to add a transaction that already exists- Ensure it fails
//     */
//    @Test
//    @DisplayName("Test for CreateGroupTransactionController - SadCase")
//    void createGroupTransactionControllerTestSadCase() {
//        //ARRANGE
//        CreateGroupTransactionController control = new CreateGroupTransactionController();
//        Person joao = new Person("João", "Travessa de santa Bárbara", "Aveiro", "1987-04-17", null, null);
//        Group g1 = new Group("Saude", "2018-01-26", joao);
//
//        Category c1 = new Category("health");
//
//        Account debit = new Account("pills", "pay pills");
//        Account credit = new Account("pharmacy", "pharmacy from main street");
//
//        g1.addCategory("health");
//        g1.addAccount("pills", "pay pills");
//        g1.addAccount("pharmacy", "pharmacy from main street");
//        g1.addTransaction(20.50, Type.DEBIT, "2020-12-01 00:00:00", "health", c1, debit, credit);
//
//        //ACT
//        boolean result = control.addTransaction(g1, 20.50, Type.DEBIT, "2020-12-01 00:00:00", "health", c1, debit, credit);
//
//        //ASSERT
//        assertFalse(result);
//
//    }
//}