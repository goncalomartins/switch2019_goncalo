package project.controllers.web;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import project.dto.DuplicateGroupInfoDTO;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class DuplicateGroupRESTControllerTestIT extends AbstractTest {

    @Override
    @BeforeAll
    public void setUp() {
        super.setUp();
    }

    @Test
    public void duplicateGroupHappyCaseTest() throws Exception {

        //Arrange
        String uri = "/people/51010@switch.pt/groups/duplicate";

        DuplicateGroupInfoDTO infoDTO = new DuplicateGroupInfoDTO("510200", "510201", "510202");
        String inputJson = super.mapToJson(infoDTO);
        String expectedContent = "{\"members\":[\"51010@switch.pt\"],\"managers\":[\"51010@switch.pt\"],\"categories\":[\"Arbitros\"],\"groupID\":\"510201\",\"ledgerID\":\"510202\",\"description\":\"Futebol\",\"creationDate\":\"2020-01-20\"}";
        int expectedStatus = 201;

        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType
                (MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        String actualContent = mvcResult.getResponse().getContentAsString();
        int actualStatus = mvcResult.getResponse().getStatus();

        assertEquals(expectedContent, actualContent);
        assertEquals(expectedStatus, actualStatus);

    }


}