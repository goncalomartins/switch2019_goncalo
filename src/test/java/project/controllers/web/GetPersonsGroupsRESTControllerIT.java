package project.controllers.web;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class GetPersonsGroupsRESTControllerIT extends AbstractTest {

    @Override
    @BeforeAll
    public void setUp() {
        super.setUp();
    }

    /**
     * Test for getPersonsGroups
     * Happy Case
     *
     * @throws Exception
     */
    @Test
    @DisplayName("Test for getPersonsGroups - Happy Case")
    public void getPersonsGroupsHappyCaseTest() throws Exception {
        Map<String, Object> pathVariableMap = new HashMap<>();
        pathVariableMap.put("personID", "4001@switch.pt");

        String uri = UriComponentsBuilder.fromUriString("")
                .path("/people/{personID}/groups")
                .buildAndExpand(pathVariableMap)
                .toUriString();

        int expectedStatus = 200;

        JSONObject expectedContent = new JSONObject()
                .put("groupsDTOs", new JSONArray(Arrays.asList(
                        new JSONObject()
                                .put("_links", new JSONObject()
                                        .put("_self", new JSONObject().
                                                put("href", "http://localhost/groups/4501")))
                                .put("groupID", "4501")
                                .put("description", "general")
                                .put("creationDate", "2019-12-18"),
                        new JSONObject()
                                .put("_links", new JSONObject()
                                        .put("_self", new JSONObject().
                                                put("href", "http://localhost/groups/4502")))
                                .put("groupID", "4502")
                                .put("description", "general")
                                .put("creationDate", "2019-12-18"),
                        new JSONObject()
                                .put("_links", new JSONObject()
                                        .put("_self", new JSONObject().
                                                put("href", "http://localhost/groups/4504")))
                                .put("groupID", "4504")
                                .put("description", "general")
                                .put("creationDate", "2019-12-18"),
                        new JSONObject()
                                .put("_links", new JSONObject()
                                        .put("_self", new JSONObject().
                                                put("href", "http://localhost/groups/4507")))
                                .put("groupID", "4507")
                                .put("description", "general")
                                .put("creationDate", "2019-12-18"),
                        new JSONObject()
                                .put("_links", new JSONObject()
                                        .put("_self", new JSONObject().
                                                put("href", "http://localhost/groups/4508")))
                                .put("groupID", "4508")
                                .put("description", "general")
                                .put("creationDate", "2019-12-18"),
                        new JSONObject()
                                .put("_links", new JSONObject()
                                        .put("_self", new JSONObject().
                                                put("href", "http://localhost/groups/4509")))
                                .put("groupID", "4509")
                                .put("description", "general")
                                .put("creationDate", "2019-12-18"),
                        new JSONObject()
                                .put("_links", new JSONObject()
                                        .put("_self", new JSONObject().
                                                put("href", "http://localhost/groups/4510")))
                                .put("groupID", "4510")
                                .put("description", "general")
                                .put("creationDate", "2019-12-18")
                        ))
                );

        // Act
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();
        JSONObject actualContent = new JSONObject(mvcResult.getResponse().getContentAsString());

        // ASSERT
        assertEquals(expectedStatus, actualStatus);
        JSONAssert.assertEquals(expectedContent, actualContent, true);
    }

}