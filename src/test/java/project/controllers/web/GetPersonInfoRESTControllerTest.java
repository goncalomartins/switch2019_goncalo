package project.controllers.web;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.GetPersonInfoResponseDTO;
import project.frameworkddd.IUSAddMemberToGroupService;
import project.model.entities.person.Person;
import project.model.entities.shared.LedgerID;
import project.model.entities.shared.PersonID;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
public class GetPersonInfoRESTControllerTest {

    @Autowired
    private IUSAddMemberToGroupService service;

    @Autowired
    private GetPersonInfoRESTController controller;

    /**
     * Test for getPersonInfo
     * <p>
     * Happy Case
     */
    @Test
    @DisplayName("Test for getPersonInfo - Happy Case")
    public void getPersonInfoHappyCase() {
        //Arrange
        String email = "21001@switch.pt";
        String name = "João";
        String address = "Travessa Santa Bárbara";
        String birthDate = "1987-04-17";
        String birthplace = "Estarreja";

        GetPersonInfoResponseDTO expectedResponseDto = new GetPersonInfoResponseDTO(email, name,
                address, birthDate, birthplace);

        PersonID personID = new PersonID(email);
        Person person = new Person(personID, name, address, birthplace, birthDate, null, null,
                new LedgerID("1"));

        Mockito.when(service.getPersonByID(personID)).thenReturn(person);

        //Act
        Object result = controller.getPersonInfoByID(email).getBody();

        //Assert
        assertEquals(expectedResponseDto, result);
    }
}
