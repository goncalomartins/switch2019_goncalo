package project.exceptions;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.entities.ledger.Ledger;

import static org.junit.jupiter.api.Assertions.*;

class LedgerNotFoundExceptionTest {

    /**
     * Test for constructor filled
     * Happy case
     */
    @Test
    @DisplayName("Test for constructor filled - Happy case")
    void constructorFilledTest() {
        //ARRANGE
        LedgerNotFoundException ex = new LedgerNotFoundException("This is an error message");

        // ACT
        //ASSERT
        assertTrue(ex instanceof LedgerNotFoundException);
    }

    /**
     * Test for constructor unfilled
     * Happy case
     */
    @Test
    @DisplayName("Test for constructor unfilled - Happy case")
    void constructorUnfilledTest() {
        //ARRANGE
        LedgerNotFoundException ex = new LedgerNotFoundException();

        // ACT
        //ASSERT
        assertTrue(ex instanceof LedgerNotFoundException);
    }

    /**
     * Test for getMessage with filled constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for getMessage with filled constructor - Happy case")
    void getMessageFilledConstructorHappyCase() {
        //ARRANGE
        LedgerNotFoundException ex = new LedgerNotFoundException("This is an error message");
        String expected = "This is an error message";

        // ACT
        String actual = ex.getMessage();

        //ASSERT
        assertEquals(expected, actual);
    }

    /**
     * Test for getMessage with unfilled constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for getMessage with unfilled constructor - Happy case")
    void getMessageUnfilledConstructorHappyCase() {
        //ARRANGE
        LedgerNotFoundException ex = new LedgerNotFoundException();
        String expected = "Ledger not found";

        // ACT
        String actual = ex.getMessage();

        //ASSERT
        assertEquals(expected, actual);
    }

}