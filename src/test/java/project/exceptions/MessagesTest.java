package project.exceptions;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

class MessagesTest {

    /**
     * Test for constructor
     * Ensure exception when called
     */
    @Test
    @DisplayName("Test for constructor - Ensure exception when called")
    void constructorFilledTest() {
        //ARRANGE
        // ACT
        //ASSERT
        assertThrows(IllegalStateException.class, () -> {
            Messages test = new Messages();
        });
    }

}